<?php
error_reporting(E_ALL ^ E_NOTICE); 

if ($_SERVER['PHP_SELF']=='/tatag-viz/php/brands.php') {
	header('content-type: text/plain');
	chdir("../");
}

require_once "config.php";
require_once "php/Utils.php";

DBquery::init($dbs, array("tatagsim"));

$sql = "SELECT * FROM brands ORDER BY brand_id ASC";
$rows = DBquery::get($sql);
foreach($rows AS $r) $brands["".$r['brand_id']] = $r;

$brands = json_encode($brands, JSON_NUMERIC_CHECK);
if ($_SERVER['PHP_SELF']=='/tatag-viz/php/brands.php') exit($brands);

