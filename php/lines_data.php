<?php
error_reporting(E_ALL ^ E_NOTICE);
header('content-type: text/plain');
chdir("../");

require_once "config.php";
require_once "php/Utils.php";
DBquery::init($dbs, array("tatagsim"));

$params = array();
if (isset($_GET['countryCode'])) {
	$countryCode = "AND country_code=?";
	$params[] = $_GET['countryCode'];
}
else $countryCode = '';

if (isset($_GET['areaCode'])) {
	$areaCode = "AND area_code=?";
	$params[] = $_GET['areaCode'];
}
else $areaCode = '';

if (isset($_GET['typeSystem'])) {
	$typeSystem = "AND type_system=?";
	$params[] = $_GET['typeSystem'];
}
else $typeSystem = '';

//make sure number of markers match the number of parameter values
if ($params) $params =  array_merge($params, $params);

$sql = "SELECT 'inflow' as flowType, WEEKOFYEAR(r.updated) AS week, t.brand_id as brand, SUM(amount) as amount
FROM records r
JOIN accounts f ON r.from_acct=f.account_id
JOIN accounts t ON r.to_acct=t.account_id
JOIN brands tb ON t.brand_id=tb.brand_id
WHERE txntype='pn' AND f.brand_id != t.brand_id $countryCode $areaCode $typeSystem
GROUP BY week, t.brand_id

UNION ALL
SELECT 'outflow' AS flowType, WEEKOFYEAR(r.updated) AS week, f.brand_id as brand, SUM(amount) as amount
FROM records r
JOIN accounts f ON r.from_acct=f.account_id
JOIN brands fb ON f.brand_id=fb.brand_id
JOIN accounts t ON r.to_acct=t.account_id
WHERE txntype='pn' AND f.brand_id != t.brand_id $countryCode $areaCode $typeSystem
GROUP BY week, f.brand_id

ORDER BY week ASC, amount DESC"; 

$rows = DBquery::get($sql, $params);
if (!$rows) exit('[]'); 


foreach($rows AS $r) {
	$data[$r['flowType']]["".$r['brand']][] = array('week'=>$r['week'], 'amount'=>$r['amount']);
}

exit(json_encode($data, JSON_NUMERIC_CHECK));

