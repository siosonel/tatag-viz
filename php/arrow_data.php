﻿<?php
error_reporting(E_ALL ^ E_NOTICE);
header('content-type: text/plain');
chdir("../");

require_once "config.php";
require_once "php/Utils.php";

DBquery::init($dbs, array("tatagsim"));

$sql = "SELECT WEEKOFYEAR(r.updated) AS week, t.brand_id as to_brand, SUM(amount) as amount
FROM records r
JOIN accounts f ON r.from_acct=f.account_id
JOIN accounts t ON r.to_acct=t.account_id
WHERE txntype='pn' AND f.brand_id != t.brand_id
GROUP BY week, t.brand_id
ORDER BY week ASC, amount DESC";

$rows = DBquery::get($sql);
if (!$rows) exit('[]'); 


foreach($rows AS $r) {
	$data["".$r['to_brand']][] = $r;
}

exit(json_encode($data, JSON_NUMERIC_CHECK));

//PhlatMedia::write(array($data));

