<?php include "config.php"; ?><!DOCTYPE html>
<html>
<head>
	<title>VizLines</title>
	
	<link rel="icon" type="image/png" href="/ui/css/logo5.png" />	
	<meta name="viewport" content="width=device-width, initial-scale=1" />
	<meta http-equiv="Content-Type" content="text/html;charset=utf-8"/>
	
	<link rel="stylesheet" href="css/normalize.css">
	<link type='text/css' rel='stylesheet' href="css/lines.css" />
	<link rel="stylesheet" href="/common2/lib/foundation-5.3.3/css/foundation.min.css">
	<link rel="stylesheet" href="/common2/lib/foundation-5.3.3/icons/foundation-icons.css">
	
	<script type="text/javascript" src="/common2/lib/jQuery/jquery-2.1.0.min.js"></script>
	<script type="text/javascript" src="lib/d3/d3.v3.min.js"></script>
	<script type="text/javascript" src="lib/colorbrewer.js"></script>
	
	<script type="text/javascript" src="js/utils.js"></script>
	<script type="text/javascript" src="lib/e4/e4.js"></script>
	<script type="text/javascript" src="lib/e4/e4_chart_helpers.js"></script>
	<script type="text/javascript" src="lib/e4/esd3pathgen.js"></script>
	
	<style>
		#controlPanel label, #controlPanel input {
			display: inline;
		}
	</style>
</head>
<body>
	<div id='viz-container'>
		<div id='controlPanel' class='row' style='background-color: #eeeeee; max-width: 100%;'>
			<div class='small-6 columns'>
				<label for='typeSystem'>Brand Type</label>
				<select id='typeSystem' onchange='app()'>
					<option value='sim'>Sim</option>
					<option value='gov'>Government</option>
					<option value='for-profit'>For-profit</option>
					<option value='nonprofit'>Nonprofit</option>
				</select>
			</div>
			<div class='small-6 columns'>
				<label for='areaCode'>Area Code</label>
				<select id='areaCode' onchange='app()'>
					<option value='206'>206 (Seattle Area)</option>
					<option value='425'>425 (Bellevue Area)</option>
					<option value='253'>253 (Tacoma Area)</option>
					<option value='415'>415 (San Francisco Area)</option>
					<option value='650'>650 (South Bay Area)</option>
					<option value='408'>498 (San Jose, Santa Clara Area)</option>
				</select>
			</div>
		</div>
		<div id='lines'></div>
		<div id='legend'></div>
		<div id='toolTip'></div>
	</div>
	<script type="text/javascript" src="js/lines_app.js"></script>
	<script type="text/javascript" src="js/lines.js"></script>
	<script>	
		var app = lines_app("<?php echo TATAG_DOMAIN ?>", {
			typeSystem: 'sim',
			areaCode: 206,
			controlsDisplay: ''
		});
		
		app
		.wrappers({ 
			'lines': {width: 430, height: 400, margin: {top: 30, left: 50, bottom: 30, right: 40}}
		})
		.viz({
			timeSeries: lines(app)
		});
	
	</script>	
</body>
</html>