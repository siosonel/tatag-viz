function lines(app) {
	var refs= app.refs(), params = app.params();
	var serverData={}, fName='', currData;
	
	var lineLabelers = getLineLabelers();
	var charts = [getChartSpecs("inflow")]; //, getChartSpecs("outflow")];
	
	var charter = app.mgr
		.pathgen(esd3pathgen)
		.zoomOk('est_range')
		.minMaxGrp({
			minRef: 'self', maxRef: 'self', 
			grp: {"inflow": [], "_": []}, 
			active: ["inflow", "_"] //the filter strings to use when determining applicable min-max ranges
		})
		.minMaxCalc( [30,39] )	
		.toolTip({
			elem: ["#lines"], 
			fxn: lineToolTip
		})
		
		/*.legend({
			fxn: legendFxnCls({
				src: {maxRows: 50}
			})
		})*/			 
	
	
	var colors = getColor();	
	var currLine, ptFocus;
	var currMinMax = [60,0];
	
	function main() {
		var fName = params.typeSystem;
		
		if (serverData[fName]) processServerData(serverData[fName]); 
		else d3.json(app.dataURL + '/inflow/byWeek?typeSystem='+ '' /*params.typeSystem*/ +'&areaCode='+ params.areaCode +'&db='+ params.db, processServerData);
	}
	
	
	function getChartSpecs(d) {  
		var first=d.substr(0,1), label = d.replace(first, first.toUpperCase()); 
	
		return {
			chartId: d, // +"_"+ verNum,				
			elems: [],
			bindkey: function (d) {return d.id},
			title: {
				className: "chartTitle",
				text: function () {
					return label+' amounts of '+ params.typeSystem +' brands in area code '+ params.areaCode +', <br />over weeks in 2015'
				}
			},
			plotArea: {
				className: 'plotArea'
			},
			svg: {
				plotclip: 1,
				viewBox: '0 0 500 400',
				preserveAspectRatio: 'none',
				height: '60%',
				width: '100%'
			},
			xAxis: { //tick: 5, gridClass: 'xGrid',
				ticks: 5,
				labelText: function (d) {return 'Week #'+ d +' of 2015'},
				labelClass: 'xAxisLabel', 
				labelText: 'Week Number in 2015'
			},
			yAxis: {
				ticks: 5, 
				gridClass: 'yGrid',
				labelClass: 'yAxisLabel', 
				labelText: 'Amount'
			},
			duration: 1000
			, postUpdate: postUpdateFxn(d)
			, mouseFxn: lineLabelers[d].mouse
		}
	}
	
	function processServerData(response) {
		if (response) serverData[fName] = response;
		currData = serverData[fName]['@graph'][0].data;
		charts[0].elems = [];
		//charts[1].elems = [];
		
		currData.map(setSeriesElem);		
		render('lines');
	}
	
	function setSeriesElem(d) {
		if (!d || !d.inflow) return;

		currMinMax[0] = 60;
		currMinMax[1] = 0;
		d.inflow.map(detectMinMax);
		d.inflow.sort(sortByWeek);

		charts[0].elems.push({
			type: "line", 
			dset: d.inflow, 
			bindkey: bindKeyFxn,				
			
			name: refs.brands[d.brand_id].name,
			id: 'line_'+d.brand_id, 
			className: "flowLine", 
			gClass: 'gFlowLine',
			
			x: 'week', y: 'amount',
			stroke: lineStroke(d.brand_id)
		});
	}

	function detectMinMax(d) {
		if (d.week < currMinMax[0]) currMinMax[0] = d.week;
		if (d.week > currMinMax[1]) currMinMax[1] = d.week;
	}

	function sortByWeek(a,b) {
		return b.week - a.week;
	}
	
	function bindKeyFxn(d) {
		return d.id;
	}
	
	function lineStroke(brandId) {
		return colors[brandId%colors.length];
	}
	
	function render(mainWrapper) { console.log(currMinMax);
		charter.minMaxCalc([1,10]); //currMinMax);
		
		var c = d3.select("#"+mainWrapper)
			.selectAll("."+mainWrapper+"_chart")
			.data(charts, function (d) {return d.chartId}); 
				
		c.exit().remove();
		
		c.enter()
			.append("div")
				.attr("id", function (d) { return d.chartId +"_"+ mainWrapper })
				.attr("class", mainWrapper+"_chart")
				.call(charter)		
		
		charter.update(mainWrapper);	
	}
	
	function lineToolTip(d) {			
		var target = d3.event.target; 
		var cls = !target.className ? '' : target.className.baseVal;
		if (currLine && (cls=='plotArea' || target.id=='ptFocus')) target = currLine; 
		
		var parent = target.parentNode && target.parentNode.__data__ ? target.parentNode : null;
		
		if (cls!='flowLine' && (!currLine || (cls!='plotArea' && d3.event.target.id!='ptFocus'))) {
			hideToolTip(parent); 
			return;
		}

		var m = d3.mouse(target.parentNode.parentNode.parentNode), 
		d = target.parentNode.__data__;

		if (d.fxn && d.fxn.ptClosest) var pt = d.fxn.ptClosest(d, m, 45, target); 
		else {hideToolTip(); return;}
		
		if (pt==-1) {hideToolTip(); return;}
		
		var week = 'Week: '+pt[0], amount='Amount: '+pt[1].toFixed(2);
		
		app.toolTip(d3.event, d.name+"<br />\n"+week+"<br />\n"+amount, $(target).css('stroke'));
		
		if (currLine != target) $(currLine).css('stroke-width', '');
		currLine = target;
		$(target).css('stroke-width', '3px');
		
		if (ptFocus) ptFocus.remove();
		ptFocus = d3.select(target.parentNode)
			.append('circle')
			.attr("id", "ptFocus")
			//.attr("class", d.className)
			.attr('r',5)
			.attr("cx", d.xScale(pt[0]))
			.attr("cy", d.yScale(pt[1]))
			.style("fill", "none")
			.style("stroke", stroke)
			.style("stroke-width", 4)
			.style("stroke-opacity", 1)
	}
	
	function hideToolTip(parent) {		
		$(currLine).css('stroke-width', '');
		$('#ptFocus').remove();		
		app.toolTip.hide();
		currLine = null;		
		
		if (parent && parent.__data__ && parent.__data__.mouseFxn) {
			parent.__data__.mouseFxn(parent, parent.className.baseVal);
		}
	}
	
	function getLineLabelers() {
		var labelerSpecs = {};
		
		function getYtext(d) {return d.amount}	
		function getXsrc(d) {return d.week}
		function getXtext(d) {return '\u25C0'+' Week #'+d+' \u25B6'}		
		function labelerXValAdj(xVal) {return xVal}	
		function getTextAnchor(d) {return d>16 ? 'end' :  'start'}
		
		function labelerPostShow(key, xVal) {
			//$('.lnmx_year_line').css('display','none');
			//if (!key || arguments.length<2 || params.fullSize) return;
			for(var grp in lineLabelers) {
				if (grp!=key) lineLabelers[grp].show(xVal)
			}
		}
		
		function labelerPostHide(key) {
			//$('.lnmx_year_line').css('display','')
			//if (!key || params.fullSize) return;
			for(var grp in lineLabelers) {
				if (grp!=key) lineLabelers[grp].hide(key)
			}
		}
			
		function labelerGuideClick() {
			for(var grp in lineLabelers) lineLabelers[grp].reorder()
		}
		
		function labelerSetFxn(grp) {
			var data = []
			function addData(d) {data.push(d)}
			function labelerSelectionFilter(d) {return this.style.display!='none'}
			
			return function () { 
				var	chartId = '#'+grp+'_lines';
					
				this.mainGrp = d3.select(chartId).select(".chart_mainGrp");
				this.plotNode = this.mainGrp.select('.plotArea').node();
				
				data = []				
				this.mainGrp.selectAll('.dataSeries').selectAll('.gFlowLine').filter(labelerSelectionFilter).each(addData)
				this.data = data;
				
				var mainNode = this.mainGrp.node();
				if (mainNode) {
					this.xScale = mainNode.__data__._e4_xScale;
					this.yScale = mainNode.__data__._e4_yScale;
				} 
			}
		}
		
		function mouseFxn(grp) {
			return function (target,cls) {if (cls!='plotArea') lineLabelers[grp].show(); else lineLabelers[grp].hide()}
		}
		
		var fxns = {}, grps = {inflow:'', outflow:''}
		for(var grp in grps) {
			labelerSpecs[grp] = {
				key: grp, plotSelector: '.plotArea', xValDefault: 10,
				bindKey: bindKeyFxn, className: 'lineLabel', 
				
				x:getXsrc, xText: getXtext, xValAdj: labelerXValAdj, xMaxBuffer: 10,
				y:getYtext, yText: getYtext,
				
				r:5, width: 30, height: 18, bgcolor: 'rgba(250,250,250,0.7)',	//line labels dimension, styles			
				
				textAnchor: getTextAnchor, 
				
				lineStroke: '#ccc', lineY2: 400,				
				guideWidth: 70, guideHeight: 22, guideFill: 'rgba(240,240,240,0.9)',
				
				postShow: labelerPostShow, postHide: labelerPostHide,
				
				guideClick: labelerGuideClick,				
				set: labelerSetFxn(grp),
				duration: 300
			}
			
			fxns[grp] = e4_lineLabel(labelerSpecs[grp]); 
			fxns[grp].mouse = mouseFxn(grp);
		}
		
		return fxns
	}
	
	function postUpdateFxn(grp) {
		return function () {lineLabelers[grp]();}		
	}
	
	function getColor() {
		var temp=['#000', '#f00', '#0f0', '#00f', '#f0f'], i=0, j=0, c = [0,120,240, 60,180,300];
		
		for(i=0; i<60; i+=10) {
			for(j=0; j<6; j++) temp.push("hsl("+ (i+c[j]) +',50%,50%)')
		} 

		return temp;
	}
	
	return main;
}