function chord_m(baseURL, divElem, dataURL) {
	var refs={};
	
	var serverData={}, currData;
	
	var width = 600,
			height = 600,
			outerRadius = Math.min(width, height) / 2 - 10,
			innerRadius = outerRadius - 24;

	var formatPercent = d3.format(".1%");
	var arc, layout, path, svg;	
	
	var colors = colorbrewer.RdYlBu[11].concat(colorbrewer.PuBuGn[9], colorbrewer.BrBG[11], colorbrewer.PiYG[11]);	
	var toolTip = $.toolTip({top: [400,-150,-150], left: [900,-50,-50], strokeBy: 'stroke'});
	
	function main(e) {
		params.weekNum = d3.select('#weekNum').property('value');
		
		if (serverData[params.weekNum]) processServerData(); 
		else d3.json(dataURL(), processServerData);
	}
	
	function build() {
		arc = d3.svg.arc()
			.innerRadius(innerRadius)
			.outerRadius(outerRadius);

		layout = d3.layout.chord()
				.padding(.04)
				.sortSubgroups(d3.descending)
				.sortChords(d3.ascending);

		path = d3.svg.chord()
				.radius(innerRadius);

		svg = d3.select(divElem).append("svg")
				.attr("width", width)
				.attr("height", height)
			.append("g")
				.attr("id", "circle")
				.attr("transform", "translate(" + width / 2 + "," + height / 2 + ")");
			
		svg.append("circle")
				.attr("r", outerRadius)
				.style('fill', 'none');
	}
	
	
	function processServerData(response) {
		if (response) serverData[params.weekNum] = response['@graph'][0];
		currData = serverData[params.weekNum];
		
		svg.selectAll(".group").remove();
		svg.selectAll(".chord").remove();
		
		// Compute the chord layout.
		layout.matrix(currData.matrix);

		// Add a group per source-brand.
		var group = svg.selectAll(".group")
				.data(layout.groups, groupId);
		
		group = group.enter().append("g")
				.attr("class", "group")
				.attr('id', groupId)
				.on("mouseover", mouseover);

		// Add a mouseover title.
		group.append("title").text(mouseoverTitle);

		// Add the group arc.
		var groupPath = group.append("path")
				.attr("id", function(d, i) { return "group" + currData.brands[d.index]; })
				.attr("d", arc)
				.style("fill", function(d, i) { return colors[i] });

		// Add a text label.
		var groupText = group.append("text")
				.attr("x", function (d,i) {
					var textfit = 2+innerRadius*(d.endAngle - d.startAngle) / 12; //roughly arc-length / font-size
					return textfit > 2 ? 6 : i%2==0 ? -2 : 4;
				})
				.attr("dy", function (d,i) {
					var textfit = 2+innerRadius*(d.endAngle - d.startAngle) / 12; //roughly arc-length / font-size
					return textfit < 3 ? 5 : 15;
				}); 

		groupText.append("textPath")
				.attr("xlink:href", function(d, i) { return "#group" + currData.brands[d.index]; })
				.text(function(d, i) { 
					var textfit = 2+innerRadius*(d.endAngle - d.startAngle) / 16; //roughly arc-length / font-size
					return refs.brands[currData.brands[d.index]].alias.substr(0, Math.round(textfit));
				});

		// Remove the labels that don't fit. :(
		//groupText.filter(function(d, i) { return groupPath[0][i].getTotalLength() / 2 - 16 < this.getComputedTextLength(); })
				//.remove();

		// Add the chords.
		var chord = svg.selectAll(".chord")
				.data(layout.chords, chordId)
			
		chord.exit().remove();
		
		chord.selectAll('path')
			.style("fill", function(d, i) { return colors[d.source.index] })
			.attr('d', path);
		
		chord.enter().append("path")
				.attr("class", "chord")
				.attr("id", chordId)
				.style("fill", function(d, i) { return colors[d.source.index] })
				.attr("d", path);		
		
		// Add an elaborate mouseover title for each chord.
		chord.append("title").text(function(d) {
			var b = currData.brands, sourceId = b[d.source.index], targetId = b[d.target.index];
			return refs.brands[sourceId].alias
					+ " -> "+ refs.brands[targetId].alias
					+ ": " + d.source.value.toFixed(2)
					+ "\n " + refs.brands[targetId].alias
					+ " -> " + refs.brands[sourceId].alias
					+ ": " + d.target.value.toFixed(2);
		});

		function mouseover(d, i) {
			chord.classed("fade", function(p) {
				return p.source.index != i
						&& p.target.index != i;
			});
		}
	};
	
	function groupId(d) {
		return 'source_'+ currData.brands[d.index];
	}
	
	function chordId(d) {
		return 'st_'+ currData.brands[d.source.index] +'_'+ currData.brands[d.target.index];
	}
	
	function mouseoverTitle(d, i) {
		return refs.brands[currData.brands[i]].name;
	}
	
	function resize() {		
		var h = $('#chord').height();
		width = Math.min(600, $('#chord').width() - 20, h && h>300? h: 600);
		height = width;
		outerRadius = Math.min(width, height) / 2 - 10;
		innerRadius = outerRadius - 24;
		
		
		toolTip.hide().posStatic({
			top: 10, left: '10%'
		});
		
		d3.select(divElem).select('svg').remove();
		build();
		main();
	}
	
	function toolTipFxn(e) {
		if (typeof e=='string' && e=='hide') {toolTip.hide(); return;}
		var tag = e.target.tagName.toUpperCase(),
			elem = tag == 'TEXT' ? document.getElementById(e.target.firstChild.href.baseVal.substr(1))
			: tag == 'TEXTPATH' ? document.getElementById(e.target.href.baseVal.substr(1))
			: null;
			
		if (elem) e.target = elem;		
		if (!e.target || !e.target.id || e.target.id.search('group')!=0) return;
		
		var id = 1*e.target.id.substr(5);
		if (!id || !refs.brands[id]) {toolTip.hide(); return;}
		
		var	html = "<div style='position: absolute; top: 0px; right: 5px;'>[<span class='fi-x large'></span>]</div>"
				+ (!parent || !parent.homeMain ? '' : "<div id='ratePrompt' style='position: absolute; top: 0px; right: 25px;'>[rate]</div>")
				+ '<br />'+refs.brands[id].name 
				+'</div>';
		
		toolTip(e, html, e.target);
		document.getElementById('toolTip').__data__ = refs.brands[id];
	}
	
	main.refs = refs;
	main.build = build;
	
	main.ready = function () {		
		var timer;
		$(window).bind('resize', function(){
			 timer && clearTimeout(timer);
			 timer = setTimeout(resize, 200);
		});
		
		$('#chord').click(toolTipFxn);
		$('#toolTip').click(main.toolTipClickHandler);
		
		$.ajax({
			url: baseURL +'/brand/collection?db=' + params.db, 
			success: function (response) {
				refs.brands = {}
				var brands = response['@graph'][0].brand;
				for(var i=0; i<brands.length; i++) {
					brands[i].alias = brands[i].name.replace('the ','').replace('The ','').substr(0,12);
					refs.brands[brands[i].brand_id] = brands[i];
				}
				
				resize();
			}
		});
	}
	
	main.toolTipClickHandler = function (e) {
		var tag = e.target.tagName.toUpperCase(),
			elem = tag == 'SPAN' || e.target.id=='ratePrompt' ? e.target : e.target.firstChild; //console.log(tag); console.log(e.target.href.baseVal);
			
		if (elem.className && elem.className.search('fi-x')!=-1) toolTipFxn('hide');
		else if (elem.id=='ratePrompt' && parent && parent.homeMain) {
			var d = document.getElementById('toolTip').__data__;
			var brand =  refs.brands[d.brand_id];		
			parent.app.openRatingsForm(brand);
		}
	}
	
	return main;
}
