if (!$) $ = {};

$.getBaseLog = function(x, y) {
    return Math.log(y) / Math.log(x);
}

$.disjoin = function(a, b) {
  return $.grep(a, function($e) { return $.inArray($e, b) == -1; });
};

$.pad = function(input, padstr) {
	return (padstr+input).substr(-1*padstr.length)
}

$.isNumber = function isNumber(n) {
	if (typeof n=='number') return 1;
  n = n.replace(/[,]/g, '');
	return !isNaN(parseFloat(n)) && isFinite(n);
}

$.arrayUnique = function(array) {
	var a = array.concat();
	for(var i=0; i<a.length; ++i) {
		for(var j=i+1; j<a.length; ++j) {
			if(a[i] === a[j]) a.splice(j--, 1);
		}
	}

	return a;
};

$.getParams = function() {
	var params = window.location.search.substring(1).split("&"),
		hash = {};

	for (var i = 0; i < params.length; i++) {
		var val = params[i].split("=");
		if (val[0]=='rankRange') { 
			var v = val[1].split(',');
			if (isNaN(+v[0])) v[0] = 1  
			if (isNaN(+v[1])) v[1] = 25
			hash[val[1]] = [+v[0], +v[1]]; 
		}
		else if (val[0]) hash[unescape(val[0])] = unescape(val[1]);
	}
	return hash;
}

$.addCommas = function (nStr) { //return nStr;
	nStr += '';
	x = nStr.split('.');
	x1 = x[0];
	x2 = x.length > 1 ? '.' + x[1] : '';
	var rgx = /(\d+)(\d{3})/;
	while (rgx.test(x1)) {
		x1 = x1.replace(rgx, '$1' + ',' + '$2');
	}
	return x1 + x2;
}

//not-so-smart text wrapping function, will fold text about half-way text length
$.wrapText = function (str, maxLen) {
	str = str.replace(/\//g," ")	
	var nominalLen = Math.ceil(str.length/2), sArr = str.split(' '), 
		prevToken = '', currToken = sArr[0];
	
	for (var i = 1; i < sArr.length + 1; i++) {		
		if (i==sArr.length || currToken.length > nominalLen) { //console.log(currToken.length+' '+maxLen+' '+nominalLen+' '+i+' '+sArr)
			if (currToken.length == str.length && currToken.length < maxLen) return [currToken]
			else if (currToken.length > nominalLen) return [prevToken, sArr.slice(i - sArr.length - 1).join(' ')];
			else return [currToken, sArr.slice(i-sArr.length).join(' ')]
		}
		else {
			prevToken = currToken;
			currToken = currToken+' '+sArr[i];
		}
	}
}

$.setCookie = function (name,value,days) {
	if (days) {
		var date = new Date();
		date.setTime(date.getTime()+(days*24*60*60*1000));
		var expires = "; expires="+date.toGMTString();
	}
	else var expires = "";

	var str = name+"="+value+expires+"; path=/";
	if (str.length > 4000) alert("The list is too long to be saved as a browser cookie. Remove or shorten one or more of the listed items to bring the string length to less than 4000 characters.")
	else document.cookie = str
}

$.getCookie = function (name) {
	var nameEQ = name + "=";
	var ca = document.cookie.split(';');
	for(var i=0;i < ca.length;i++) {
		var c = ca[i];
		while (c.charAt(0)==' ') c = c.substring(1,c.length);
		if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
	}
	return null;
}

$.delCookie = function (name) {
	setCookie(name,"",-1);
}

$.toolTip = function (align) {
	var posStatic = {}, target; 
	
	function main(e, content, arg3) { if (!e) return;
		target = e.target
		
		var w = $('#toolTip').html(content).width(),
			h = $('#toolTip').height(),
			pos = {left: e.pageX, top: e.pageY};
			stroke = arguments.length == 3 && typeof arg3 != 'string' && align.strokeBy ? $(arg3).css(align.strokeBy) 
				: arguments.length == 3 ? arg3 : '';
		
		if (!stroke || stroke=='none') {
			if ($(arg3).css('stroke')) stroke= $(arg3).css('stroke')
			else stroke = '#000'
		}
		
		$('#toolTip').css({
			//opacity: 0,
			display: "block",
			top: 'top' in posStatic ? posStatic.top
				: pos.top < align.top[0] ? pos.top + align.top[1] : pos.top - h + align.top[2], //, - 55 - origXY.top,
			left: 'left' in posStatic ? posStatic.left
				: pos.left < align.left[0] ? pos.left + align.left[1] 
				: pos.left - w + align.left[2], //+ 23 - origXY.left,
			border: '2px solid '+ stroke
		})//.animate({opacity: 1})
	}
	
	main.hide = function () {
		$('#toolTip').css('display','none');
		return main;
	}
	
	main.align = function (obj) {
		if (!arguments.length) return align;
		align = obj;
		return main;
	}
	
	main.posStatic = function (obj) {
		if (!arguments.length) return posStatic;
		posStatic = obj;
		return main;
	}
	
	main.target = function () {return target}
	
	return main
}
