function chord(divElem, dataURL, mainKey, otherKey) {	
	var serverData = {};
			
	var w = 600,
			h = 680,
			rx = w / 2,
			ry = h / 2,
			m0,
			rotate = 0;

	var splines = [];

	var cluster = d3.layout.cluster()
			.size([360, ry - 120])
			.sort(function(a, b) { return d3.ascending(a.key, b.key); });

	var bundle = d3.layout.bundle();

	var line = d3.svg.line.radial()
			.interpolate("bundle")
			.tension(.85)
			.radius(function(d) { return d.y; })
			.angle(function(d) { return d.x / 180 * Math.PI; });

	// Chrome 15 bug: <http://code.google.com/p/chromium/issues/detail?id=98951>
	var div = d3.select(divElem).append('div')
			.style("top", "0px")
			.style("left", "0px")
			.style("width", w + "px")
			.style("height", w + "px")
			.style("position", "relative")
			.style("-webkit-backface-visibility", "hidden");

	var svg = div.append("svg:svg")
			.attr("width", w)
			.attr("height", w)
		.append("svg:g")
			.attr("transform", "translate(" + rx + "," + ry + ")");

	svg.append("svg:path")
			.attr("class", "arc")
			.attr("d", d3.svg.arc().outerRadius(ry - 120).innerRadius(0).startAngle(0).endAngle(2 * Math.PI))
			.on("mousedown", mousedown);

	d3.select(window)
			.on("mousemove", mousemove)
			.on("mouseup", mouseup);
	
	
	function main(e) {
		params.tickNum = d3.select('#tickNum').property('value');
		
		if (serverData[params.tickNum]) processData(serverData[params.tickNum]); 
		else d3.json(dataURL(), processData);
	}

	function processData(classes) {
		serverData[params.tickNum] = classes;
		
		var nodes = cluster.nodes(packages.root(classes)),
				links = packages.imports(nodes),
				splines = bundle(links);

		var path = svg.selectAll("path.link")
				.data(links)
			
		path.enter().append("svg:path")
				.attr("class", function(d) { return "link source-" + d.source.key + " target-" + d.target.key; })
				.attr("d", function(d, i) { return line(splines[i]); });
		
		path.exit().remove();
		
		var nodes = svg.selectAll("g.node")
				.data(nodes.filter(function(n) { return !n.children; }))
		
		nodes.enter().append("svg:g")
				.attr("class", "node")
				.attr("id", function(d) { return "node-" + d.key; })
				.attr("transform", function(d) { return "rotate(" + (d.x - 90) + ")translate(" + d.y + ")"; })
			.append("svg:text")
				.attr("dx", function(d) { return d.x < 180 ? 8 : -8; })
				.attr("dy", ".31em")
				.attr("text-anchor", function(d) { return d.x < 180 ? "start" : "end"; })
				.attr("transform", function(d) { return d.x < 180 ? null : "rotate(180)"; })
				.text(function(d) { return d.key; })
				.on("mouseover", mouseover)
				.on("mouseout", mouseout);
		
		nodes.exit().remove();
		
		d3.select("input[type=range]").on("change", function() {
			line.tension(this.value / 100);
			path.attr("d", function(d, i) { return line(splines[i]); });
		});
	}
	
	
	function mouse(e) {
		return [e.pageX - rx, e.pageY - ry];
	}

	function mousedown() {
		m0 = mouse(d3.event);
		d3.event.preventDefault();
	}

	function mousemove() {
		if (m0) {
			var m1 = mouse(d3.event),
					dm = Math.atan2(cross(m0, m1), dot(m0, m1)) * 180 / Math.PI;
			div.style("-webkit-transform", "translateY(" + (ry - rx) + "px)rotateZ(" + dm + "deg)translateY(" + (rx - ry) + "px)");
		}
	}

	function mouseup() {
		if (m0) {
			var m1 = mouse(d3.event),
					dm = Math.atan2(cross(m0, m1), dot(m0, m1)) * 180 / Math.PI;

			rotate += dm;
			if (rotate > 360) rotate -= 360;
			else if (rotate < 0) rotate += 360;
			m0 = null;

			div.style("-webkit-transform", null);

			svg
					.attr("transform", "translate(" + rx + "," + ry + ")rotate(" + rotate + ")")
				.selectAll("g.node text")
					.attr("dx", function(d) { return (d.x + rotate) % 360 < 180 ? 8 : -8; })
					.attr("text-anchor", function(d) { return (d.x + rotate) % 360 < 180 ? "start" : "end"; })
					.attr("transform", function(d) { return (d.x + rotate) % 360 < 180 ? null : "rotate(180)"; });
		}
	}

	function mouseover(d) {
		if (mainKey=='target') svg.selectAll("path.link."+ mainKey +"-" + d.key)
				.classed(mainKey, true)
				.each(updateNodes(otherKey, true));

		else svg.selectAll("path.link."+ otherKey +"-" + d.key)
				.classed(otherKey, true)
				.each(updateNodes(mainKey, true));
				
		if (mainKey=='target') chordOutflow.mouseover(d, 1)
	}

	function mouseout(d) {
		if (mainKey=='target') svg.selectAll("path.link."+ mainKey +"-" + d.key)
				.classed(mainKey, false)
				.each(updateNodes(otherKey, false));

		else svg.selectAll("path.link."+ otherKey +"-"+ d.key)
				.classed(otherKey, false)
				.each(updateNodes(mainKey, false));
		
		if (mainKey=='target') chordOutflow.mouseout(d, 1)
	}

	function updateNodes(name, value) {
		return function(d) {
			if (value) this.parentNode.appendChild(this);
			svg.select("#node-" + d[name].key).classed(name, value);
		};
	}

	function cross(a, b) {
		return a[0] * b[1] - a[1] * b[0];
	}

	function dot(a, b) {
		return a[0] * b[0] + a[1] * b[1];
	}

	main.mouseover = mouseover;
	main.mouseout = mouseout;
	
	return main
}
	