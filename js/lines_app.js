/*
	Purpose: Set up app controls, viz functions
	Author: Edgar Sioson
	Date: 2014-07-30
*/

function lines_app(dataURL, defaults) {
	var refs={}, params=$.extend({db:''}, defaults, $.getParams());
	var controls=function () {}, controlElems = [];
	
	var mgr = e4();
	var viz={};
	
	$(document).ready(setBrands)
	
	function main(e) {	
		params.typeSystem = $('#typeSystem').val();
		params.areaCode = $('#areaCode').val();
		viz.timeSeries();
	}
	
	function setBrands() {
		$('#controlPanel').css('display', params.controlsDisplay);
		$('#typeSystem').val(params.typeSystem);
		$('#areaCode').val(params.areaCode);
		
		$.ajax({
			url: dataURL +'/brand/collection?db=' + params.db, 
			success: function (response) {
				refs.brands = {}
				var brands = response['@graph'][0].brand;
				for(var i=0; i<brands.length; i++) refs.brands[brands[i].brand_id] = brands[i];
				main();
			}
		})
	}
	
	function linkParams() {}
	
	function errorHandler(xhr, status, text) {
		console.log("Ajax error ["+ status +': '+ text + ']')
		//alert("Error in server request. Please reload your browser. ["+ status +': '+ text + ']');
	}
	
	main.dataURL = dataURL;
	main.mgr = mgr;
	
	main.refs = function (str) {
		if (!arguments.length) return refs;
		if (str == 'reset') setRefs();
		return main;
	}
	
	main.params = function (obj) {
		if (!arguments.length) return params;
		for (var prop in obj) params[prop] = obj[prop];
		return main;
	}
	
	main.controls = function (fxn) {
		if (!arguments.length) return controls;
		controls = fxn;
		return main;
	}
	
	main.controlElems = function (arg) {
		if (!arguments.length) return controlElems;
		if (typeof arg == 'string') controlElems.push(arg);
		else if (Array.isArray(arg)) {
			for(var i=0; i<arg.length; i++) {
				if (controlElems.indexOf(arg[i])==-1) controlElems.push(arg[i]);
			}
		}
		
		return main;
	}
	
	main.wrappers = function (obj) {
		if (!arguments.length) return mgr.dimensions();
		mgr.dimensions(obj);
		return main;
	}
	
	main.viz = function (obj) {
		if (!arguments.length) return viz
		for (var fxnName in obj) viz[fxnName]  = obj[fxnName]
		return main
	}
	
	main.waitMask = function (str) {
		if (str!='show') $('#waitMask').css('display','none')
		else {
			$('#waitMask').css({height: $('body').css('height')});	
			d3.select('#waitMask').style('opacity',0.01).transition().duration(2000).style('opacity',0.95)
			$('#waitMask').css({display: 'block'});	
		}
	}	
	
	main.errorHandler = errorHandler
	main.toolTip = $.toolTip({top: [400,10,-50], left: [900,-50,0], strokeBy: 'fill'});
	
	return main
}