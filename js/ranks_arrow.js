﻿function ranks_arrow(app) {
	var refs=app.refs(), params=app.params(), mgr=app.mgr;
	var currElems=[];
	
	//var controlElems = ['rankBy', 'sex', 'metric', 'age', 'geo', 'rankRange', 'colsArr'];
	var colors = {
		"gov": 'rgba(255,0,0,0.35)', 
		"for-profit": 'rgba(0,255,0,0.35)', 
		"nonprofit": 'rgba(0,0,255,0.35)', 
		"sim": 'rgba(255,0,255,0.35)'
	};
	
	var metricAlias = {
		'inflow': 'Inflow Amount',
		'outflow': 'Outflow Amount',
		'arRatio': 'Accepted:Rejected Ratio'
	};
	
	var colorBinArr = ["gov", "for-profit", "nonprofit", "sim"];
	
	var serverData={}, fName='';
	
	var arrows = e4_arrow()
		.wrapperDiv('#arrow') //has to be valid d3 selector notation or object(s)
		.rowKey('brand').valKey('amount') //could use string property name instead of index#, depending on processed data structure
		.layout({
			rowHeight: 22, 
			colInterval: 300, //colInterval is x-position of each column of rect's
			colTitleX: 45,
			rectHeight: 20, 
			rectWidth: 250, 
			textX: 5, textY: 15, connectorY: 10, //relative to rect x,y position
			rankMinOffset: -5, rankMaxOffset: 5,
			hiddenY: 2000
		})
		.chartTitle(function () {
			var metric=metricAlias[params.metric];
		
			if (params.rangeBy=='minMax') 
				return "<h4>Ranks by "+ metric +", top "+ (params.rankMin+1) +" to "+ (params.rankMax+1) +'</h4>';
			else {
				if (!params.idSearchVal) {
					$('#nameSearchVal').val('amazon');
					app.getBrandId();
				}
				
				return "<h4>Relative Rank  of "+ refs.brands[params.idSearchVal].name.substr(0,12) +" by "+ metric +" Amount</h4>";
			}
		})
		.textID(function (d,i) {return 'arrowText_'+ d.brand +'_'+d.week;})
		.rectID(function (d,i) {return 'arrowRect_'+ d.brand +'_'+d.week;})
		.rectFill(function (d,i) {return colors[refs.brands[d.brand].type_system]})
		.lineID(function (d,i) {return 'arrowLine_'+ d.brand +'_'+d.week;})
		.strokeColor(function (d,i) {return colors[refs.brands[d.brand].type_system]})
		.cellTextVal(function (rank,d,i) {
			var name=refs.brands[d.brand].name, alias = name<20 ? name : name.substr(0,19) +'...';
			return (rank+1)+' '+ alias +' '+d.amount.toFixed(2);
		})
		//.toolTipFxn(toolTipFxn)
	
	var toolTip = $.toolTip({top: [400,30,-50], left: [900,-50,-50], strokeBy: 'stroke'});
	
	function main(e) {
		if (!isActive(e)) return;
		
		fName = params.metric;
		
		if (fName in serverData) processServerData();
		else $.ajax({
			url: app.baseURL + "/"+ params.metric +"/byWeek?db=" + params.db,
			cache: false,
			dataType: 'json',
			success: processServerData,
			error: app.errorHandler
		})
	}
	
	function isActive(e) { return 1;
		if (params.activeViews.indexOf('arrow')==-1) {
			$('#arrow').css('display','none');	return;
		}
		
		if (e=='exit') return;
		
		$('#arrow').css('display','inline-block');	
		app.controlElems(controlElems);
		toolTip.hide();		
		return 1;
	}
	
	function processServerData(response) {
		if (arguments.length) serverData[fName] = response['@graph'][0].data;		
		
		currElems=[]; 
		
		//re-arrange data rows to each have column sequence that match the layout implied by colsArr
		serverData[fName].map(function (d) {currElems.push(setByWeek(d[params.metric]));});
		
		arrows
			.rankMinMax(params.rankMin, params.rankMax)
			.idSearchVal(params.idSearchVal)
			.numCols(params.colsArr.length)
			.colTitlesArr(params.colsArr)
			
		main.render();
		d3.selectAll('.arrows_svg').transition().duration(2000).style('height', 80+arrows.rankLength()*22);
		colorBins();
	}
		
	function setByWeek(rowdata, i) {
		var arr = [];
		for(var i=0; i< params.colsArr.length; i++) {
			for(var j=0; j<rowdata.length; j++) {
				if (rowdata[j] && rowdata[j].week==1*params.colsArr[i]) {arr.push(rowdata[j]);}
			}
		} 
		
		return arr
	}
	
	function getRankRange(cellVal) {
		
	}
	
	function toolTipFxn(e) {
		if (typeof e=='string' && e=='hide') {toolTip.hide(); return;}
		if (e.target && e.target.className.baseVal=='arrowColTitle') return;
		
		var d = e.target.__data__;
		if (!d || !refs.brands[d.brand]) {toolTip.hide(); return;}
		
		var	html = "<div style='position: absolute; top: 0px; right: 5px;'>[<span class='fi-x large'></span>]</div>"
				+ (!parent || !parent.homeMain ? '' : "<div id='ratePrompt' style='position: absolute; top: 0px; right: 25px;'>[rate]</div>")
				+ '<div>Period #'+ d.week +'<br />'
				+ refs.brands[d.brand].name +'<br />'+ metricAlias[params.metric] +': '+ d.amount
				+'</div>';
		
		toolTip(e, html, e.target);
		document.getElementById('toolTip').__data__ = d;
	}
	
	function colorBins() {
		var bins = d3.select('#colorBins').selectAll('.colorBin')
			.data(colorBinArr, colorBinId)
		
		bins.exit().remove();
		
		var b = bins.enter().append('div')
			.attr('id', colorBinId)
			.attr('class', 'colorBin')			
			.style('background-color', colorBinFill)
			.html(colorBinId)
	}
	
	function colorBinFill(d) {
		return colors[d];
	}
	
	function colorBinId(d) {
		return d;
	}
	
	main.toolTipFxn = toolTipFxn;
	
	main.docReady = function () {
		arrows.docReady()
	}
	
	main.controlElems = function (arr) { 
		if (!arguments.length) return controlElems; 
		if (Array.isArray(arr)) controlElems = arr;
		return main
	}
	
	main.render = function () {
		var divWidth = $('#arrow').css('width');
		if (typeof divWidth=='string') divWidth = divWidth.search('px')!=-1 ? 1*divWidth.slice(0,-2) : 1*divWidth;		
		var propWidth = divWidth/(2*params.colsArr.length-1);
		if (params.colsArr.length==1) propWidth = propWidth - 100; //right margin
		
		arrows.layout({
			colInterval: Math.max(100, propWidth+100), 
			rectWidth: Math.max(150, propWidth)
		});
		
		arrows(currElems);
	}
	
	return main
}
