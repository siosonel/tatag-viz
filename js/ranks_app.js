/*
	Purpose: Set up app controls, viz functions
	Author: Edgar Sioson
	Date: 2014-07-30
*/

function ranks_app(baseURL, defaults) {
	var refs={}, params=$.extend({db:''}, defaults, $.getParams());
	var controls=function () {}, controlElems = [];
	var viz={};	
	var mgr = e4().pathgen(e4_pathgen).zoomOk('plotArea', -1);
	var twoColumnVal = defaults.colsArr.join(',');
	
	$(document).ready(init);
	
	function main(e) {	
		params.metric = $('#metric').val();
		params.rangeBy = $('input[name=rangeBy]:checked').val();	
		$('#rankMinMaxBox').css('display', params.rangeBy=='minMax' ? 'block' : 'none');		
		$('#idSearchValBox').css('display', params.rangeBy=='brandId' ? 'block' : 'none');
		
		params.idSearchVal = params.rangeBy=='brandId' ? 1*$('#idSearchVal').val() : 0;			
		params.colsArr= $('#colsArr').val().split(',');
		params.rankMin = 1*$('#rankMin').val() - 1;
		params.rankMax = 1*$('#rankMax').val() - 1;
		params.rankBy = 'amount';
		viz.arrow();
	}
	
	function init() {
		$('#controlPanel').css('display', params.controlsDisplay);
		$('#metric').val(params.metric);
		$('#rangeBy-'+ params.rangeBy).prop('checked', true);
		$('#idSearchVal').val(params.idSearchVal);
		$('#rankMin').val(""+(1*params.rankMin+1));
		$('#rankMax').val(""+(1*params.rankMax+1));
		
		$.ajax({
			url: baseURL +'/brand/collection?db=' + params.db, 
			success: function (response) {
				refs.brands = {}
				var brands = response['@graph'][0].brand;
				for(var i=0; i<brands.length; i++) refs.brands[brands[i].brand_id] = brands[i];
				resize();
			}
		});
		
		$(window).resize(resize);
		$('#arrow').click(viz.arrow.toolTipFxn);
		$('#toolTip').click(main.toolTipClickHandler);
	}
	
	function linkParams() {}
	
	function errorHandler(xhr, status, text) {
		console.log("Ajax error ["+ status +': '+ text + ']')
		//alert("Error in server request. Please reload your browser. ["+ status +': '+ text + ']');
	}
	
	function resize() {
		if (1*$('#viz-container').width() < 400) {
			$('#colsArr').val( params.colsArr[params.colsArr.length-1] );
			twoColumnVal = params.colsArr.length > 1 ? params.colsArr.join(',') : twoColumnVal;
			$('#arrow').css('min-height','350px');
		}
		else {
			$('#colsArr').val( twoColumnVal );
			$('#arrow').css('min-height','450px');
		} 
		
		main();
	}
	
	main.baseURL = baseURL;
	main.mgr = mgr;
	
	main.refs = function (str) {
		if (!arguments.length) return refs;
		if (str == 'reset') setRefs();
		return main;
	}
	
	main.params = function (obj) {
		if (!arguments.length) return params;
		for (var prop in obj) params[prop] = obj[prop];
		return main;
	}
	
	main.controls = function (fxn) {
		if (!arguments.length) return controls;
		controls = fxn;
		return main;
	}
	
	main.controlElems = function (arg) {
		if (!arguments.length) return controlElems;
		if (typeof arg == 'string') controlElems.push(arg);
		else if (Array.isArray(arg)) {
			for(var i=0; i<arg.length; i++) {
				if (controlElems.indexOf(arg[i])==-1) controlElems.push(arg[i]);
			}
		}
		
		return main;
	}
	
	main.wrappers = function (obj) {
		if (!arguments.length) return mgr.dimensions();
		mgr.dimensions(obj);
		return main;
	}
	
	main.viz = function (obj) {
		if (!arguments.length) return viz;
		for (var fxnName in obj) viz[fxnName]  = obj[fxnName];
		return main;
	}
	
	main.waitMask = function (str) {
		if (str!='show') $('#waitMask').css('display','none')
		else {
			$('#waitMask').css({height: $('body').css('height')});	
			d3.select('#waitMask').style('opacity',0.01).transition().duration(2000).style('opacity',0.95)
			$('#waitMask').css({display: 'block'});	
		}
	}	
	
	main.getBrandId = function () {
		var name = $('#nameSearchVal').val().toLowerCase();
		for(var id in refs.brands) {
			if (refs.brands[id].name.toLowerCase().search(name) != -1) { //console.log(refs.brands[id].brand_id);
				$('#idSearchVal').val(refs.brands[id].brand_id);
				if (!arguments.length) main();
				return;
			} 
		}
		
		alert("There are no brands with the name similar to '"+ name +"'.");
	}
	
	main.toolTipClickHandler = function (e) {
		var elem =  e.target.tagName.toUpperCase() == 'SPAN' || e.target.id=='ratePrompt' ? e.target : e.target.firstChild;
		if (elem.className && elem.className.search('fi-x')!=-1) viz.arrow.toolTipFxn('hide');
		else if (elem.id=='ratePrompt' && parent && parent.homeMain) {
			var d = document.getElementById('toolTip').__data__;
			var brand =  refs.brands[d.brand];
			parent.app.openRatingsForm(brand);
		}
	}
	
	main.errorHandler = errorHandler
	
	return main
}