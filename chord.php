<!DOCTYPE html>
<html>
<head>
	<title>VizChord</title>
	<meta http-equiv="Content-Type" content="text/html;charset=utf-8"/>
	<!--<link type="text/css" rel="stylesheet" href="style.css"/>-->
	<style type="text/css">
.flowDiv {
	display: inline-block;
	width: 600px;
}

path.arc {
  cursor: move;
  fill: #fff;
}

.node {
  font-size: 10px;
}

.node:hover {
  fill: #1f77b4;
}

.link {
  fill: none;
  stroke: #1f77b4;
  stroke-opacity: .4;
  pointer-events: none;
}

.link.source, .link.target {
  stroke-opacity: 1;
  stroke-width: 2px;
}

.node.target {
  fill: #d62728 !important;
}

.link.source {
  stroke: #d62728;
}

.node.source {
  fill: #2ca02c;
}

.link.target {
  stroke: #2ca02c;
}

</style>
</head>
<body>
	<div>
		<br />
		<label for='weekNum'>Tick Number&nbsp;&nbsp;</label><input type='text' name='weekNum' id='weekNum' value="1" onchange='app()' />
	</div>
	<h2>Transactions between brands</h2>
	<div id='chordInflow' class='flowDiv'></div>
	<div id='chordOutflow' class='flowDiv'></div>
	<div style="position:absolute;bottom:0;font-size:18px;">tension: <input style="position:relative;top:3px;" type="range" min="0" max="100" value="85"></div>
	<script type="text/javascript" src="lib/d3/d3.v3.min.js"></script>
	<script type="text/javascript" src="js/chord_packages.js"></script>
	<script type="text/javascript" src="js/chord.js"></script>
	<script>
		var params = {weekNum: 1};
		
		var chordInflow = chord('#chordInflow', function () {
			return "api/flow/matrix.php?weekNum="+ params.weekNum
		}, 'target', 'source');
		
		var chordOutflow = chord('#chordOutflow', function () {
			return "php/chord_data.php?weekNum="+ params.weekNum +'&key=source'
		}, 'source', 'target');
		
		function app() {
			chordInflow();
			chordOutflow();
		}
		
		app();
		
	</script>
  </body>
</html>