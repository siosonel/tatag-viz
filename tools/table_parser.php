<?php
header('content-type: text/plain');
set_time_limit(0);

//$csv = str_getcsv(file_get_contents('../resources/country-codes.csv'), ); print_r($csv);
$byIso3 = array();
if (($handle = fopen('../resources/country-codes.csv', "r")) !== FALSE) {
	$header = fgetcsv($handle, 1000, ",");
	while (($row = fgetcsv($handle, 1000, ",")) !== FALSE) {
		$countries[] = array_combine($header, $row);
		if (file_exists("../resources/area_codes/". $row[3] .".json")) {$byIso3[] = $row; echo $row[3] ."\n";}
	}
}

if ($saveAs=$_GET['saveAs']) {
	file_put_contents("../resources/$saveAs", json_encode($byIso3));
}
else {
	foreach($countries AS $country) { 
		sleep(1);
		$data = array();
		$iso3 = $country['ISO3166-1-Alpha-3']; echo "\nhttp://countrycode.org/$iso3";
		$dial = '+'.$country['Dial'];
		$html = file_get_contents("http://countrycode.org/$iso3");
		$table = substr($html, strpos($html,'<div id="child_center_column">'));
		$table = substr($table, 0, strpos($table, '<div id="child_right_column">'));
		$rows = explode("<tr>", $table); 

		foreach($rows AS $r) {
			$cells = explode('<td ', trim($r));
			if (count($cells)>1 AND isset($cells[2])) {  
				$data[trim(strip_tags('<td '.$cells[1]))] = trim(str_replace($dial, '', str_replace('&nbsp;','',trim(strip_tags('<td '.$cells[2])))));		
			}
		} 

		if ($data) {
			file_put_contents("../resources/data/$iso3.json", json_encode($data, JSON_NUMERIC_CHECK));
			echo " ... saved $iso3 $dial\n";
		} 
	}
}
