﻿E4 - README

======================================================
E4 is a framework for developing visualizations in d3
======================================================


Version 0.3

    Copyright (C) 2010-2014 Edgar Sioson <edgar -at- gmail.com>
    Released as free software under terms of the MIT License

	
Requirements

    d3.js
		

Summary

    E4 is a framework for developing visualizations using the excellent d3.js library. 
		The primary use case is	for generating multiple charts with ease, with the core e4 engine taking 
		care of data binding, min-max calculations, and rendered element enters, exits, and updates.
		
		The framework tackles the common annoyances related to handling multiple charts of the same type, 
		such as when creating side-by-side comparisons. It simplifies using the same dataset for different
		chart types by following d3's option of assigning data-getter functions to values. It has a 
		unified approach for assigning unique element IDs, exclusion of series plots which affect 
		min-max calculations, and creating framework extensions as new series plot types within 
		a shared svg.
		
		In the application codebase that uses e4, the resulting pattern is the focus on building specifications 
		as data object arrays for different charts and series types. These chart and series specifications 
		will be fed into e4 which will automatically handle calculating multi-chart scales, and 
		series enters, exits, and updates.
		
		It could be contrasted from highcharts as being multi-chart oriented and also being based
		on d3. It is similar to highcharts in that it expects nested data objects as input to the core
		engine. 
		

Core Details
  
		1. A wrapper div will hold one or more general viz type such as a chart, pie, etc.
		
			a. A dimensions object, which will be applied to each 'wrapped' chart, is optionally
			specified for each wrapper div when instantiating e4. For example:
			
				var Renderer = e4()
					.wrappers({
						"main_charts":{width: 400, height: 290, margin: {top: 5, right: 20, bottom: 20, left: 50}}, 
						"preview_charts":{width: 200, height: 130, margin: {top: 5, right: 20, bottom: 20, left: 50}}
					})
		
		2. Data is bound to the wrapper div as an array of chart data objects:
		
			  var wrapperData = [chartDataObject1, chartDataObject2, chartDataObject3, ...]
		
		  a. The caller app handles the binding of the chart data objects to the wrapper div. 
		
				var charts = d3.select('#main_charts').data(wrapperData)
		
			b. Entering chart selections should be assigned an ID and class by the app, and 
				then be fed into the e4 main function. 
				
				charts.enter()
					.attr('id', function (chartDataObj) {...}) //optional
					.attr('class', 'main_charts_chart') //required
				
			c. Exiting charts are removed by the app, not e4.
		
				charts.exit()
					.remove() //app-specified behavior, may include transitions
		
		  d. Updates are called on the remaining chart selection inside the wrapper.
			
				charts.update()
					
				-- OR --
				
				Renderer
					.minMaxCalc() //optional, but recommended, recalculation of min-max as data may have changed
					.update('#'+wrapperID)
		
		3. Each chart data object must be constructed as follows:
		
			chartDataObject = {
				chartId: "...", //or a function			
				elems: [seriesDataObject1, seriesDataObject2, ...], //series elements to plot
				bindkey: function (d) { ... }, //optional for determining entering, exiting data series
				
				title: { //optional chart title object
					className: "chartTitle",
					text: "..." // or a function
				},
				plotArea: { //optional - forms the background under the plotted series types
					className: 'plotArea'
				},				
				svg: { //default to creating a clipPath
					plotclip: 1
				}, 
				xAxis: { //optional
					tick: 4, 
					tickFormat: // function for formatting tick labels
				},				
				yAxis: { //optional
					tick: 5, 
					tickFormat: function (d) {return d.toFixed(3)}, //example
					gridClass: "...", 
					labelClass: "...", 
					labelText: "..."
				},
				duration: 1000, //transition duration for entering/exiting serieses
				postUpdate: //optional call back function after this chart has been rendered 
			}
		
		4. Each series data object must be constructed as follows:
		
			seriesDataObject = {
				type: "line/area/pt_pathD/pt_circle/pt_rect/text/segment/pie/stack_v/...", //specify one 
				dset: [...], //array of values 
				bindkey: function (d) {...}, //optional for determining entering, exiting data points
				
				name: "...", //any name, id, or className value may be specified as a function
				id: "...",  
				className: "...", 
				
				//the following are type-dependent series data object properties for  
				//specifying each plotted value in a series; a value may be specified as
				//a dataset row's property name or array index, or a data-getter function
				x: , y:                      //line series 
				x: , y0: , y1:               //area
				x: , y:                      //pt_pathD
				cx: , cy:                    //pt_circle
				x: , y: , width: , height:   //pt_rect
				
				
				//style options
				fill: , stroke: , stroke-width: 
			}


			
Min-Max Calculation //optional feature

    E4 has a built-in min-max calculator and tracking mechanism. 		
		(details to be documented)	
		
Download

    if you could read this, you already have access to the source code
	 
	
Installation

    Load http://tyaga.org/NPX/install.php on your browser and follow the on-screen instructions.  

	
ChangeLog

    See code repository
	

	
Documentation

    This is it, plus comments in the code. See examples in the wild.

	
Support

    Email me with questions, suggestions, etc.


Credits

    Thanks to Mike Bostock for 

