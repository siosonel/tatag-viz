﻿/*
Purpose: Set up wrapper and pathgen functions as needed to use e4
Author: Edgar Sioson
Date: 2014-09-05
*/


function e4_heat() {
	//default layout
	var wrapperDiv = '' //has to be valid d3 selector notation or object(s)
	var rowKey = 0; //could use string property name instead of index#, depending on processed data structure
	var colKey = 1;
	var refCol = [];
	var valKey = 0 //could use string property name instead of index#, depending on processed data structure
	var rankMin = 0, rankMax = 9; //zero-index ranks
	var geoRef = 0;
	var geoCols = [];
	var colTitleTextFxn = function () {return ''};
	var colorGrps={}
	
	var layout = { //defaults
		rowHeight: 22, colInterval: 30, //colInterval is x-position of each column of rect's
		colTitleX: 45,
		rectHeight: 20, rectWidth: 32, 
		textX: 15, textY: 15, connectorY: 10, //relative to rect x,y position
		rankMinOffset: -5, rankMaxOffset: 5,
		hiddenY: 2000
	}
		
	var wrapperData = [
		getWrapperData('heat_0') //, getWrapperData('cause_1'), getWrapperData('risk_0'), getWrapperData('risk_1')
	];
	
	
	var nameCol = {
		type: 'arrow',
		dset: [],
		bindkey: bindKeyFxn,
		
		id: 'heatCol_name',
		className: 'heatCol',
		
		transform: 'translate(35,'+ 50 +')',
		cellText: {
			id: rowLabelID, bindKey: rowLabelID, 
			className: 'rowLabel',
			val: nameColText, 
			x: -10, y: textY, 
			anchor: 'end'
		},
		hiddenY: layout.hiddenY,
		duration: 3000
	}
	
	var numCols = 0; 
	var sortedByRef = []; //filtered list of ranked causes or risks to those that appear within rankMin-rankMax within any displayed year
	
	
	
	var hoveredG; //tracks hovered element for highlighting connectors and rects
	var toolTipFxn = function () {}
	var clickedCauses = []
	
	$(document).ready(function () {$(wrapperDiv).mouseover(hoverFxn)});
	
	//expects data to be array of connectable 'row' data arrays
	function main(data, refArr) {
		if (!wrapperDiv) {console.log("Null value for wrapperDiv."); return;}
		
		mainData = data;
		setRefCol(refArr);		
		
		nameCol.dset = refArr;
		wrapperData[0].elems = [nameCol]
		mainData.map(setHeatCol)
		draw();
	}
	
	function getWrapperData(chartId) {
		return {
			chartId: chartId,				
			elems: [],		
			bindkey: bindKeyFxn,
			//x: 0, y: 0,
			title: {
				className: "chartTitle",
				text: '', 
			},
			plotArea: {
				className: 'plotArea'
			},
			svg: {className: 'heat_svg'},
			mask: 'none'
		}
	}
	
	function setRefCol(refArr) {
		refCol = []		
		for(var i=0; i<refArr.length; i++) refCol.push(refArr[i][rowKey]) 
	}
	
	function setHeatCol(colData) {
		wrapperData[0].elems.push({
			type: 'arrow',
			dset: colData,
			bindkey: bindKeyFxn,
			
			id: 'heatCol_'+ colData[0][colKey],
			className: 'heatCol',
			
			transform: colTransform,
			cellRect: {
				id: rectID, bindKey: rectID,
				className: 'cellRect',
				height: layout.rectHeight, width: layout.rectWidth, 
				x: 0, y: rectY, 
				fill: rectFill, fillOpacity: 0.8, stroke: '#ececec'
			},
			cellText: {
				id: textID, bindKey: textID, 
				className: 'cellText',
				val: textVal, 				
				x: layout.textX, y: textY, 
				anchor: 'middle'
			},
			colTitle: {
				data: [colData[0]],
				id: 'heatCol_'+colData[0][colKey]+'_title',
				className: 'heatColTitle',
				transform: colTitleTransform, 
				text: colTitleTextFxn,
				stroke: colTitleColor
			},
			hiddenY: layout.hiddenY,
			duration: 3000
		})
	}
	
	function bindKeyFxn(d) {
		return d.id
	}
	
	function colTransform(d,i) {
		return 'translate('+ (1+geoCols.indexOf(d.dset[0][colKey]))*layout.colInterval +',50)'
	}
	
	function colTitleID(d,i) {
		return 'heatText_title_'+d
	}
	
	function colTitleColor(d,i) { //console.log(d[colKey] + '---' + params.geoRef)
		return d[colKey]==params.geoRef ? 'rgb(226, 91, 8)' : '';
	}
	
	function colTitleTransform(d,i) {
		return 'rotate(320 0,0)'
	}
	
	function nameColText(d,i) {
		return refs[params.rankBy][d[0]][4]
	}
	
	function rectID(d,i) {
		return 'heatRect_'+ d[colKey] +'_'+ d[rowKey];
	}
	
	function rectX(d,i) {
		return (1+geoCols.indexOf(d[colKey]))*layout.colInterval
	}
	
	function rectY(d,i) {
		//if (i>numCols-1) return layout.hiddenY;
		
		var rank = refCol.indexOf(d[rowKey]), offset=0;
		if (rank==-1 || rank<rankMin || rank>rankMax) return layout.hiddenY;
		return rank*layout.rowHeight + offset;
	}
	
	function rectFill(d) {
		return colorGrps[params.rankBy][d[9]];
	}
	
	function strokeColor(d,i) {
		return colorGrps[params.rankBy][i].replace();
	}
	
	function textID(d,i) {
		return 'heatText_'+ d[colKey] +'_'+ d[rowKey];
	}
	
	function textVal(d,i) {
		var rank = i; 
		//if (!refs[params.rankBy][d[0]]) {return;}
		return (rank+1) //+' '+refs[params.rankBy][d[0]][4]
	}
	
	/*function textX(d,i) {
		return rectX(d,i) + layout.textX
	}*/
	
	function textY(d,i) {
		return rectY(d,i) + layout.textY;
	}
	
	function rowLabelID(d,i) {
		return 'heatRowLabel_'+ d[rowKey];
	}
	
	function draw() { //console.log(wrapperData[0].elems)
		var heatmap = d3.select(wrapperDiv)
			.selectAll('.heat_chart')
			.data(wrapperData, function (d) { return d.chartId })
		
		heatmap.enter().append('div')
			.attr('class', 'heat_chart')
			.call(app.mgr)
			
		heatmap.call(app.mgr.update)		
	}
	
	function hoverFxn(e) {
		if (!e) return;
		var cls = e.target.className.baseVal;
		if (cls=='rowLabel' || cls=='heatColTitle') return;
	
		if (hoveredG) {		
			hoveredG.style('stroke-width','').style('fill', rectFill)
		}
	
		if (['rect','text'].indexOf(e.target.tagName)==-1 || e.target.parentNode.className.baseVal!='heatCol') {
			hoveredG = null; toolTipFxn('hide'); return;
		}
		
		hoveredG = d3.select('#'+e.target.id.replace('Text', 'Rect'))
		hoveredG.style('stroke-width','3px').style('fill', '#fff')
		toolTipFxn(e)
	}
	
	function clickFxn(e) {
		if (['tspan'].indexOf(e.target.tagName)==-1) return;
		var p = e.target.parentNode;
		if (p.className.baseVal!='heatColTitle') return;
		
		params.geoRef = 1*p.id.split('_')[1];
		app();
	}
	
	main.docReady = function () {
		$(wrapperDiv).mouseover(hoverFxn).click(clickFxn)
	}
	
	main.wrapperDiv = function (arg) {
		if (!arguments.length) return wrapperDiv
		wrapperDiv = arg
		return main
	}
	
	main.numCols = function (num) {
		if (!arguments.length) return numCols
		numCols = num
		return main
	}
	
	main.rowKey = function (strOrInt) {
		if (!arguments.length) return rowKey
		rowKey = strOrInt
		return main
	}
	
	main.colKey = function (strOrInt) {
		if (!arguments.length) return valKey;
		colKey = strOrInt;
		return main;
	}
	
	main.colTitleTextFxn = function (fxn) {
		if (!arguments.length) return colTitleTextFxn;
		colTitleTextFxn = fxn
		return main
	}
	
	main.rankMinMax = function (min, max) {
		if (!arguments.length) return [rankMin, rankMax]
		rankMin = min; rankMax = max;
		return main
	}
	
	main.geoRef = function (numOrStr) {
		if (!arguments.length) return geoRef
		geoRef = numOrStr
		return main
	}
	
	main.geoCols = function (arr) {
		if (!arguments.length) return geoCols
		geoCols = arr
		return main
	}
	
	main.layout = function (obj) {
		if (!arguments.length) return layout;
		for(var prop in obj) layout[prop] = obj[prop]
		return main
	}
	
	main.toolTipFxn = function (fxn) {
		if (!arguments.length) return toolTipInfo;
		toolTipFxn = fxn
		return main
	}
	
	
	main.colorGrps = function (obj) {		
		if (!arguments.length) return colorGrps
		for(var grpId in obj) {			
			colorGrps[grpId] = []
			obj[grpId].map(function (d) { //alert(JSON.stringify(d)); return;
				if ('range' in d) {
					var j = d.range[0]
					while(j <= d.range[1]) { 
						colorGrps[grpId].push(d.color)
						j++
					}
				}
				else if ('parentList' in d) {
					
				}
				else colorGrps[grpId].push(d.color)
			})
		} //console.log(colorGrps)
		
		return main
	}
	
	main.rectFill = rectFill
	
	return main
}