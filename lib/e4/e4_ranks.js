/*
Purpose: Set up wrapper and pathgen functions as needed to use e4
Author: Edgar Sioson
Date: 2014-09-05
*/

if (!e4_pathgen) var e4_pathgen = {}

e4_pathgen.arrow = (function arrowSeries() {	
	function addColTitle(d, title) { //highly optional, mostly intended for heatmap; e4_arrow uses rowData instead				
		title.attr('id', d.colTitle.id)
			.attr('class', d.colTitle.className)
			.attr('transform', d.colTitle.transform)
			.style('stroke', d.colTitle.stroke)
			.html(d.colTitle.text)
		
		return title
	}
	
	function updateColTitle(d, title) { 
		title.style('stroke', d.colTitle.stroke)
		title.transition().duration(d.duration).attr('transform', d.colTitle.transform)
	}
	
	function addRects(d, rects, opts) {
		rects.attr('id', d.cellRect.id)
			.attr('class', d.cellRect.className)
			.attr('x', d.cellRect.x)
			.attr('y', opts && 'y' in opts ? opts.y : d.hiddenY) 
			.attr('height', d.cellRect.height)
			.attr('width', d.cellRect.width)
			.attr('class', d.cellRect.className)
			.style('fill', d.cellRect.fill)
			.style('fill-opacity', d.cellRect.fillOpacity)
			.style('stroke', d.cellRect.stroke)
			
		if (opts && 'opacity' in opts) rects.style('opacity',opts.opacity[0]).transition().duration(d.duration).style('opacity', opts.opacity[1])
		
		return rects
	}
	
	function updateRects(d, rects) {
		var r = rects.transition().duration(d.duration)
		r.attr('x', d.cellRect.x)
		r.attr('y', d.cellRect.y) 
		r.attr('height', d.cellRect.height)
		r.attr('width', d.cellRect.width)
		r.style('opacity', 1)
		
		var rdelay = rects.transition().delay(d.duration+500)
		rdelay.style('fill-opacity', d.cellRect.fillOpacity)
		rdelay.style('fill', d.cellRect.fill)
	}
	
	function updateFill(rects) { //d3 transition().delay() starts fill color as black
		rects.style('fill', d.cellRect.fill)
	}
	
	function addTexts(d, texts, opts) {
		texts.attr('id', d.cellText.id)
			.attr('class', d.cellText.className)
			.attr('text-anchor', d.cellText.anchor)
			.attr('x', d.cellText.x)
			.attr('y',  opts && 'y' in opts ? opts.y : d.hiddenY)
			.style('fill', d.cellText.fill)
			.style('font-weight',d.cellText.fontWeight)
			.text(d.cellText.val)
		
		if (opts && 'opacity' in opts) texts.style('opacity',opts.opacity[0]).transition().duration(d.duration).style('opacity', opts.opacity[1])
		
		return texts
	}
	
	function updateTexts(d, texts) {
		duration = d.duration;	
		var t = texts.style('font-weight',d.cellText.fontWeight).transition().duration(d.duration)
		
		t.attr('x', d.cellText.x)
		t.attr('y', d.cellText.y)
		t.style('opacity', 1)
		texts.text(d.cellText.val) //transition().delay(d.cellText.delay).text(d.cellText.val)
	}
	
	function addLines(d, lines) {
		lines.attr('id', d.connector.id)
			.attr('class', d.connector.className)
			.attr('x1', d.connector.x1)
			.attr('y1', d.hiddenY)
			.attr('x2', d.connector.x2)
			.attr('y2', d.hiddenY)
			.style('stroke', d.connector.stroke)
			.style('stroke-dasharray', d.connector.strokeDashArray)
			.style('opacity', 0)
		
		return lines
	}
	
	function updateLines(d, lines) {
		var l = lines.transition().duration(d.duration)
				
		l.attr('x1', d.connector.x1)
			.attr('y1', d.connector.y1)
			.attr('x2', d.connector.x2)
			.attr('y2', d.connector.y2)
			.style('stroke-dasharray', d.connector.strokeDashArray)
			.style('display', d.connector.display)
			
		l.style('opacity', d.connector.opacity)
	}
	
	
	return {
		draw: function (g, d, wrapperId, update) {
			g.attr('id', d.id)
				.attr('class', d.className)
				.style('opacity', 0).transition().duration(d.duration).style('opacity',1)
			
			if ('cellRect' in d) var rects = addRects(d, g.selectAll('.'+d.cellRect.className).data(d.dset, d.cellRect.bindKey).enter().append('rect'))				
			if ('cellText' in d) var texts = addTexts(d, g.selectAll('.'+d.cellText.className).data(d.dset, d.cellText.bindKey).enter().append('text'))				
			if ('connector' in d) var lines = addLines(d, g.selectAll('.'+d.connector.className).data(d.dset, d.connector.bindKey).enter().append('line'))
			if ('colTitle' in d) var title = addColTitle(d, g.selectAll('.'+d.colTitle.className).data(d.colTitle.data, d.colTitle.bindKey).enter().append('text'))
			
			if (update) g//.transition().duration(d.duration)
				.attr('transform', d.transform);
			
			if ('colTitle' in d) updateColTitle(d, title)
			if ('cellRect' in d) updateRects(d, rects)
			if ('cellText' in d) updateTexts(d, texts)
			if ('connector' in d) updateLines(d, lines)
		},
		minMax: function (xMinMax, yMinMax, d) { return yMinMax },
		update: function (g,d) {
			g.attr('transform', d.transform);
			g.transition().duration(d.duration).style('opacity',1)
			
			if ('colTitle' in d) {
				updateColTitle(d, g.selectAll('.'+d.colTitle.className).data(d.colTitle.data, d.colTitle.bindKey))
			}
			
			if ('cellRect' in d) {
				var rects = g.selectAll('.'+d.cellRect.className).data(d.dset, d.cellRect.bindKey)
				rects.exit().transition().duration(d.duration).style('opacity', 0).remove()
				
				updateRects(d, rects)
				
				var newRects = addRects(d, rects.enter().append('rect'), {y: d.cellRect.y, opacity:[0,1]})
				updateRects(d, newRects)					
			}
															
			if ('cellText' in d) {
				var texts = g.selectAll('.'+d.cellText.className).data(d.dset, d.cellText.bindKey)					
				texts.exit().transition().duration(d.duration).style('opacity', 0).remove()
				
				updateTexts(d, texts)
				
				var newTexts = addTexts(d, texts.enter().append('text'), {y: d.cellText.y, opacity:[0,1]})
				updateTexts(d, newTexts)
			}
				
			if ('connector' in d) {
				var lines = g.selectAll('.'+d.connector.className).data(d.dset, d.connector.bindKey)
				lines.exit().transition().duration(d.duration).style('opacity', 0).remove()
				
				updateLines(d, lines)					
				
				var newLines = addLines(d, lines.enter().append('line'))
				updateLines(d, newLines)
			}
		},
		elemExit: function (g, d, wrapperId) { 
			g.transition().duration(d.duration).style('opacity', 0).remove();
			
			if ('colTitle' in d) updateColTitle(d, g.selectAll('.'+d.colTitle.className).data(d.colTitle.data, d.colTitle.bindKey))
			if ('cellRect' in d) updateRects(d, g.selectAll('.'+d.cellRect.className).data(d.dset, d.cellRect.bindKey))																
			if ('cellText' in d) updateTexts(d, g.selectAll('.'+d.cellText.className).data(d.dset, d.cellText.bindKey))					
			if ('connector' in d) updateLines(d, g.selectAll('.'+d.connector.className).data(d.dset, d.connector.bindKey))
		}
	}
})()
