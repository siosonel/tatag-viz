
/***********************************************************
Function for creating chart axes
************************************************************/
function e4_axes(minMax, scaleXY) {
	function main() {}
	
	main.enter = function (data, chartElem, g) {
		if (!data.xAxis || !data.yAxis) return;
	
		//create axes
		var xTickNum = 'xAxis' in data && 'ticks' in data.xAxis ? data.xAxis.ticks : 4,
			yTickNum = 'yAxis' in data && 'ticks' in data.yAxis ? data.yAxis.ticks : 5;
		
		chartElem.xAxis = d3.svg.axis()
			.scale(data._e4_xScale)
			.orient("bottom")
			.tickSize(6, 0)
			.ticks(xTickNum)
			.tickFormat('xAxis' in data && 'tickFormat' in data.xAxis ? data.xAxis.tickFormat : d3.format("d")) //d3.format("d"))
		
		chartElem.yAxis = d3.svg.axis()
			.scale(data._e4_yScale)
			.orient("left")
			.tickSize(6, 0)
			.ticks(yTickNum)
			.tickFormat('yAxis' in data && 'tickFormat' in data.yAxis ? data.yAxis.tickFormat : d3.format("g"))
		
		var grids = g.append('g').attr('class', 'grids')
		
		g.append("g")
			.attr("class", "xAxis")
			.attr("transform", "translate(-0.5," + (data._e4_effHeight - 0.5) + ")")
			.call(chartElem.xAxis);
			
		g.append("g")
			.attr("class", "yAxis")
			.attr("transform", "translate(-0.5,0)")
			.call(chartElem.yAxis);	
	
		//add optional grid lines
		gridStage(data, grids, 'xAxis', function (d) {return "M" + data._e4_xScale(d) + " 0L" + data._e4_xScale(d) + " -" + data._e4_effHeight})
		gridStage(data, grids, 'yAxis', function (d) {return "M0 " + data._e4_yScale(d) + "L"+ data._e4_effWidth +" "+ data._e4_yScale(d)})
		
		//add optional axis labels
		axisLabelStage(data.xAxis, chartElem)
		axisLabelStage(data.yAxis, chartElem)
	}

	function gridStage(data, g, cls, pathFxn) { 
		if (!(cls in data) || !('gridClass' in data[cls])) return;
		
		var axis = data[cls], 
			scale = data['_e4_'+ cls.replace('Axis','Scale') ],
			tickNum = 'ticks' in axis ? axis.ticks : 4; //console.log(pathFxn); console.log(scale); console.log(axis);
		
		var gridLines = g.selectAll('.'+axis.gridClass).data(scale.ticks(tickNum)); //return;
		
		gridLines.exit().remove()
		
		gridLines.enter().append('path')
			.attr('class', axis.gridClass)
			.attr('d', pathFxn)
			.style('opacity', 0)
		.transition().delay(data.duration)
			.style('opacity', 'opacity' in axis ? axis.opacity : 1); //return;
			
		gridLines.transition()
			.attr('d', pathFxn)
			.style('opacity', 0)
		.transition().delay(data.duration)
			.style('opacity', 'opacity' in axis ? axis.opacity : 1);		
	}

	function axisLabelStage(axis, chartElem, updateBool) {
		if (!axis || !('labelClass' in axis)) return;
		var label = updateBool 
			? d3.selectAll('.'+axis.labelClass)
			: d3.select(chartElem).append('div')
		
		label.attr('class', axis.labelClass).html(axis.labelText)
	}

	main.update = function (data, chartElem, chartId, wrapperId, s) {		
		if (!data.xAxis || !data.yAxis) return;
		
		var chartDiv = d3.select(chartElem),
			g = chartDiv.select('svg').select('.chart_mainGrp');
							
		if ('xAxis' in chartElem) {			
			var x = chartDiv.selectAll('.xAxis').transition().duration(data.duration)				
			x.call( chartElem.xAxis.scale(data._e4_xScale) );
			x.attr("transform", "translate(-0.5," + (data._e4_effHeight - 0.5) + ")")
			
			axisLabelStage(data.xAxis, chartElem, 1)				
			gridStage(data, g, 'xAxis', function (d) {return "M" + data._e4_xScale(d) + " 0L" + data._e4_xScale(d) + " -" + data._e4_effHeight})
		}
		
		if ('yAxis' in chartElem) {
			var y = chartDiv.selectAll('.yAxis').transition().duration(data.duration)			
			y.call(chartElem.yAxis.scale(data._e4_yScale));
			y.attr( "transform", "translate(-0.5,0)" )
			
			axisLabelStage(data.yAxis, chartElem, 1)			
			gridStage(data, g, 'yAxis', function (d) {return "M0 " + data._e4_yScale(d) + "L"+ data._e4_effWidth +" "+ data._e4_yScale(d)})
		}
	}
	
	return main;
}


/********************************************************** 
Function for calculating minimum, maximum for chart groups
***********************************************************/

function e4_minMax(wrappers, pathgen) {
	var xMinMax = [0,1], //default x-range to be used by all the charts on view		
		yMinMax = { //these values are used to support dynamic rescaling based on desired user view
			"minRef": "user", "maxRef": "user",
			"user": [0,1], "self":{},  
			"assigned": {}, //each property is a chart_id that is assigned to a specific min-max group for setting/getting purposes AND prevents the assigned chart from affecting any group min-max
			"grp": {}, //each propery is a substring that, if found within a non-assigned chart id, factors that chart's min-max in the calculated group's min-max
			"active": [], //substr min-max groups could be activated or inactivated through this list
			"checked": {}, "zoomed": {}
		};
		
	var	currCacheId = '', //will track the current x-range and chart-series exclusions that affects min-max calculations
		exclude = [], //chart or series plot ids that have any of the excluded string will be hidden and not updated
		axesTip = 0.05;	
	
	function main(xRange, yRange) {
		if (xRange && xRange.length == 2) xMinMax = xRange
		
		if (yRange && yRange != 'reset' && yRange.length == 2) yMinMax.user = yRange;
		else {			
			for(var grpName in yMinMax.grp) yMinMax.grp[grpName] = [];
			currCacheId =xMinMax.join("") +'_'+ exclude.join(" ")
			
			for(var wrapperId in wrappers) {
				var w = wrappers[wrapperId]; //console.log(yRange); console.log(currCacheId)
				if (!main.excluded(wrapperId) && (!yRange || yRange=='reset' || !w.cacheId || w.cacheId != currCacheId)) { //console.log(currCacheId)
					w.reset = 1
					d3.select('#'+wrapperId).selectAll('.'+wrapperId+'_chart').each(chartCalc)
					w.cacheId = currCacheId
				}
				
				w.reset = 0
			}
			
			
			for(var grpName in yMinMax.grp) {
				yMinMax.grp[grpName] = d3.extent( yMinMax.grp[grpName] );
			} 
		}	
	}
	
	function chartCalc(data) {				
		var wrapperId = this.parentNode.id,
			chartId = data.chartId +"_"+ wrapperId, 
			yArr = [], w = wrappers[wrapperId];
	
		if (!w.reset && data.chartId in yMinMax.self && yMinMax.self[data.chartId][1] && wrappers[wrapperId].cacheId == currCacheId) {
			yArr = yMinMax.self[ data.chartId ]; console.log('reused '+wrapperId+' '+currCacheId)
		}
		else {
			var temp = data.elems.map(seriesCalc); 
			for(var i=0; i<temp.length; i++) {if (Array.isArray(temp[i])) yArr.push(temp[i][0], temp[i][1])}
			yArr = d3.extent(yArr)
			yMinMax.self[ data.chartId ] = yArr;
		} 
		
		var alias = yMinMax.assigned[chartId] ? yMinMax.assigned[chartId] : '';
					
		if (alias) yMinMax.grp[alias] = yMinMax.grp[alias].concat(yArr);
		else {
			for(var grpName in yMinMax.grp) {
				if (chartId.search(grpName) != -1) yMinMax.grp[grpName] = yMinMax.grp[grpName].concat(yArr); //if (grpName!='_') console.log(chartId+' '+yArr+', '+grpName);}
			}
    }	
	}
	
	function seriesCalc(dObj) { //dObj = series data, one of possibly many in a chart		
		if (main.excluded(dObj.id) || dObj.minMaxExempt || typeof pathgen[dObj.type].minMax != 'function') return;	
		return pathgen[dObj.type].minMax(xMinMax, 0, dObj)			
	}
	
	main.x = function (chartId) {
		if (!arguments.length || !(chartId in yMinMax.zoomed)) return xMinMax
		return yMinMax.zoomed[chartId].x
	}
	
	main.y = function () {return yMinMax}	
	
	main.exclude = function (str, not) {
		if (!arguments.length) return exclude
		
		if (Array.isArray(str)) exclude = str;
		else if (!not && exclude.indexOf(str) == -1) exclude.push(str)
		else if (not && exclude.indexOf(str) != -1) exclude.splice(  exclude.indexOf(str), 1)

		return main;
	}
		
	//determine if the elem with the given id should be excluded from
	//min-max calculation and not displayed
	main.excluded = function (id) { 
		for(var i=0; i<exclude.length; i++) { 
			if (id.search(exclude[i]) != -1) return 1;
		}
	}
	
	//this relies on plot functions to calculate the min-max of 
	//datapoints that it plots
	main.calc = main
	
	//get the applicable min-max range for a given chart id
	//this could be group min-max, user selected, or the chart's own y-scale range
	main.get = function (chartId,wrapperId) { 
		var tempMinMax = [0.00001,1], shortId = chartId.replace('_'+wrapperId,''), 
			ref = {minRef: 0, maxRef: 1}; //variable for looping below
		
		if (shortId in yMinMax.zoomed) return yMinMax.zoomed[shortId].y
		
		for(var m in ref) {
			var i = ref[m];
		
			if (yMinMax[m]=='user') tempMinMax[i] = yMinMax.user[i];
			else if (yMinMax[m]=='self') tempMinMax[i] = yMinMax.self[ chartId.replace('_'+wrapperId,'') ][i];
			else if (yMinMax.assigned[chartId]) tempMinMax[i] = yMinMax.grp[yMinMax.assigned[chartId]][i]
			else if (yMinMax[m]=='active') { 
				for(var j=0; j < yMinMax.active.length; j++) { 
					if (chartId.search( yMinMax.active[j] ) != -1) {
						tempMinMax[i] = yMinMax.grp[yMinMax.active[j]][i]; break;
					}
				} 
			}
		}			
		
		if (tempMinMax[0] > tempMinMax[1]) { console.log(tempMinMax+' '+yMinMax.minRef+' '+yMinMax.maxRef)
			if (yMinMax.minRef=='user') tempMinMax[1] = tempMinMax[0] < 0 ? tempMinMax[1] - tempMinMax[0] : tempMinMax[1] + tempMinMax[0]
			else if (yMinMax.maxRef=='user') tempMinMax[0] = tempMinMax[1] < 0 ? tempMinMax[0] + tempMinMax[1] : tempMinMax[0] - tempMinMax[1]
		}
		
		var tipExtra = Math.abs((tempMinMax[1] - tempMinMax[0]) * axesTip);
		return [ tempMinMax[0] - tipExtra,  tempMinMax[1] + tipExtra ]		
	}
	
	main.grp = function (d) { 
		if (!arguments.length) return yMinMax;
		for(var prop in d) { 
			if (prop=='override') { //legacy support
				if (d[prop].min=='self') yMinMax.minRef = 'self';
				else if (d[prop].min) {yMinMax.minRef = 'user'; yMinMax.user[0] = d[prop].val[0];}
				else yMinMax.minRef = "active";
				
				if (d[prop].max=='self') yMinMax.maxRef = 'self';
				else if (d[prop].max) {yMinMax.maxRef = 'user'; yMinMax.user[1] = d[prop].val[1];}
				else yMinMax.maxRef = "active";
			}
			else if (prop=='active') { //legacy support
				if (d.active=='user') {yMinMax.minRef='user'; yMinMax.maxRef='user';}
				else if (!d.active.length) {yMinMax.minRef='self'; yMinMax.maxRef='self';}
				else if (Array.isArray(d.active)) {
					yMinMax.minRef='active'; yMinMax.maxRef='active'; yMinMax.active = d.active;
				}
			}
			else if (prop) yMinMax[prop] = d[prop];
		}
	}
	
	return main
}


/*****************************************
				Zoom function
- will set the zoomed[chartId] property of a minMax.y() function (find below)
- needs to call e4 update function to trigger rescale, transition
******************************************/

function e4_zoomPlot(minMax, e4fxn) {
	var toolTipSet = e4fxn.toolTipSet,
		zoomXY = [], //saved corner points at the start of zoom area selection
		zoomArea = [], //highlighted zoom area while the other corner is being dragged
		zoomSelectOk = ['plotArea'], //list of classNames that, when clicked, will permit zoom area selection				
		zoomFactor = [1,1], //not really used, see comments for zoomAdj() below
		customHandler = {};
	
	//start of zoom sequence, when a user clicks on a blank plot area
	function main(e) { //console.log(d3.event.target.className.baseVal); console.log(zoomSelectOk); 
		if (zoomSelectOk.indexOf(d3.event.target.className.baseVal) == -1) return;
		toolTipSet(null); 
	
		zoomXY = d3.mouse(this); // zoomAdj(d3.mouse(this)),zoomAdj(d3.mouse(this));
		var svg = d3.select(this)
		
		$(this).css('cursor', 'url(css/images/cursor_select2.png), auto')
		
		svg.on('mousedown.e4fxn', null)	
			.on('mousemove', zoomDrag)
			.on("mouseup", zoomIn)
					
		zoomArea = svg.append('rect')
			.attr('x', zoomXY[0]) //- margin.left)
			.attr('y', zoomXY[1]) //- margin.top)
			.attr('width',1)
			.attr('height',1)
			.attr('class','zoomArea')
	}

	
	//adjust zoomArea rendering as the corner is being dragged
	function zoomDrag() {
		var tempXY = d3.mouse(this), // zoomAdj(d3.mouse(this)),zoomAdj(d3.mouse(this)),		
		  x = (tempXY[0] > zoomXY[0]) ? zoomXY[0] : tempXY[0],
			y = (tempXY[1] > zoomXY[1]) ? zoomXY[1] : tempXY[1]
					
		zoomArea
			.attr('x', x)
			.attr('y', y)
			.attr('width', Math.abs(zoomXY[0] - tempXY[0]))
			.attr('height', Math.abs(zoomXY[1] - tempXY[1]))
		
		//if (window.getSelection) window.getSelection().removeAllRanges(); //removes unnecessary highlights
	}
	
	//rescale chart on mouseup when the zoomArea width and height are greater than a given threshold 
	function zoomIn() {
		//removes unnecessary highlights of axis tick labels, not sure why that gets triggered
		//if (window.getSelection) window.getSelection().removeAllRanges(); 
	
		var stop = d3.mouse(this), // zoomAdj(d3.mouse(this)),
			dx = stop[0] - zoomXY[0], 
			dy = stop[1] - zoomXY[1]; 
		
		if (Math.abs(dx) > 10 && Math.abs(dy) > 10) {
			var chart = this.parentNode, 
				wrapperId = chart.parentNode.id,
				update = chart.id in customHandler ? customHandler[chart.id] : e4fxn.update;
						
			update(wrapperId, {
				x: d3.extent( [ zoomXY[0], stop[0] ] ), //d3.extent is a shortcut to sort the order of these two points from min to max 
				y: d3.extent( [ zoomXY[1], stop[1] ] )
			}, chart.id.replace("_"+wrapperId, "") );
		}
		/*else { 
			d3.select(this).select('.dataSeries')
				.attr('transform', 'translate('+ dx +','+ dy +')')
		}*/
		
		zoomXY = []
		zoomArea.remove()		
		
		v = d3.select(this)
			.on('mousedown.e4fxn', main)
			.on('mousemove', null)
			.on('mouseup', null)
		
		d3.select(this).style('cursor', '')
		
		toolTipSet(e4fxn.toolTip())		
	}
	
	main.reset = function () { //rescale to scale ranges prior to zoom-in
		var chart = this.parentNode.parentNode, wrapperId = chart.parentNode.id,
			chartId = chart.id.replace("_"+wrapperId, ""),
			update = chart.id in customHandler ? customHandler[chart.id] : e4fxn.update; //alert('481 '+chartId)
		
		d3.select(chart).select('svg')
			.on('mousedown.e4fxn', main)	
			
		d3.select(chart).selectAll('.zoomResetDiv') //,.zoomResetText')
			.on('click.e4fxn',null)
			.remove()
		
		delete minMax.y().zoomed[chartId]; 		
		update(wrapperId,0,chartId)
	}
	
	main.ok = function (d, op) {
		if (!arguments.length) return zoomSelectOk;
		
		if (Array.isArray(d)) zoomSelectOk = d
		else if (!op || op != -1) zoomSelectOk.push(d);
		else zoomSelectOk = e4fxn.disjoin(zoomSelectOk, [d])
		
		return main;
	}
	
	main.disabled = function (str) {
		if (!arguments.length) return zoomDisabled;
		else if (!op || op != -1) zoomDisabled.push(str);
		else zoomDisabled = e4fxn.disjoin(zoomDisabled, [str])
	}
	
	main.customHandler = function (obj) {
		if (!arguments.length) return customHandler
		for(var chartId in obj) customHandler[chartId] = obj[chartId]
		return main;
	}
	
	main.setResetBtn = function (chartDiv) {
		chartDiv.append('div')
			.attr('class', 'zoomResetDiv')
			.selectAll('button')
			.data(['reset']) //['-','reset','+'])
		.enter()
			.append("button")
				.attr('class', "zoomResetBtn")
				.text( function (d) { return d; })
				.on('click.e4fxn', main.reset)
	}
	
	return main;
}



/****************************************************************
Add labels to selected lines while hovering over chart plot area
*****************************************************************/

function e4_lineLabel(spec) {
	var mainGrp, lineLabelsGrp, labels;
	var plotNode, plotHeight, plotWidth, _f_;
	var mouseX, xPos, xVal, xScale, yScale;
	var xGuide, xGuideLine, xGuideRect, xGuideText;
	var halfWidth = spec.guideWidth/2;
		
	function main() {
		spec.set()
		mainGrp = spec.mainGrp; if (!mainGrp.node()) {/*console.log('Empty selection.');*/ return;}
		plotNode = spec.plotNode;
		
		plotHeight = 1*plotNode.getAttribute('height'); 
		plotWidth = 1*plotNode.getAttribute('width'); 
		if (!plotHeight) plotHeight = 1;
		if (!plotWidth) plotWidth = 1;
		
		xScale = spec.xScale;
		yScale = spec.yScale;		
		
		lineLabelsGrp =  mainGrp.select('.lineLabelsGrp');		
		if (!lineLabelsGrp.node()) lineLabelsGrp = mainGrp.append('g').attr('class', 'lineLabelsGrp');		
		
		if (!lineLabelsGrp.select('.xGuide').node()) main.addxGuide(lineLabelsGrp);
		else main.resetxGuide();
		
		labels = lineLabelsGrp.selectAll('.'+spec.className).data(spec.data, spec.bindKey)
		labels.enter().append('g').attr('class', spec.className).each(main.addLabel)
		labels.exit().remove()
		labels = lineLabelsGrp.selectAll('.'+spec.className);	
		labels.style('display', 'none');
	}
	
	function getPathD() {
		return 'M'+ xPosAdj +',0L'+ xPosAdj +','+ plotHeight 
	}
	
	main.addLabel = function (d) {
		var lineLabel = d3.select(this).attr('class', spec.className).datum(d)
				
		if (spec.r) lineLabel.append('circle')
			.attr('cx', 0)
			.attr('cy', plotHeight)
			.attr('r', spec.r)
			//.attr('class', spec.circle.className)
			.style('stroke', spec.lineStroke)
			.style('fill', d.stroke)
		
		if (spec.width) lineLabel.append('rect')
			.attr('x', 0)
			.attr('y', plotHeight)
			.attr('width', spec.width)
			.attr('height', spec.height)
			//.attr('class', spec.rect.className)
			.style('fill', spec.bgcolor)
			
		if (spec.textAnchor) lineLabel.append('text')
			.attr('x', 0)
			.attr('y', plotHeight)
			//.attr('class', spec.text.className)
			.style('fill', d.stroke)
			.text('')
	}
	
	main.addxGuide = function (lineLabelsGrp) {		
		xPos = xScale(spec.xValDefault);
		
		mainGrp.on('mouseout', main.hide);
		
		xGuide = lineLabelsGrp.append('g').attr('class','xGuide')
			//.on('mousemove', main.show)
			.on('mouseout', main.hide)
			.on('click', spec.guideClick)
			
		xGuideLine = xGuide.append('line')
			.attr('x1',0).attr('y1',0)
			.attr('x2',0).attr('y2', plotHeight)
			.style('stroke', spec.lineStroke)
		
		xGuideRect = xGuide.append('rect')	
			.attr('x', xPos - halfWidth)
			.attr('y', plotHeight) //-spec.guideHeight)
			.attr('width', spec.guideWidth)
			.attr('height', spec.guideHeight)
			.style('fill', spec.guideFill)
			.style('cursor', 'pointer')
			.style('stroke', spec.lineStroke)
		
		xGuideText = xGuide.append('text')
			.attr('x', xPos)
			.attr('y', plotHeight + spec.guideHeight - 5)
			.style('text-anchor', 'middle')
			.style('fill', '#000')
			//.style('stroke', '#ccc')			
			.style('cursor', 'pointer')
			.style('font-weight', 'bold')
			.text('\u25C0 labels \u25B6')
	}
	
	main.resetxGuide = function () {
		xGuideLine.attr('y2', plotHeight)
		xGuideRect.attr('y', plotHeight) 
		xGuideText.attr('y', plotHeight + spec.guideHeight - 5)
	}
	
	main.show = function (x) {
		if (!xScale) /*|| !plotNode.offsetParent*/ return;  
		if (arguments.length) { //console.log(spec.key)
			xVal = x;
			mouseX = xScale(xVal);
		}
		else {
			var m = d3.mouse(plotNode);
			if (m[1] < plotHeight) {main.hide(); return;}
			
			mouseX = m[0]; //console.log(mouseX)
			xVal = Math.round(xScale.invert(mouseX)); //console.log(xVal)
		} 
		
		xPos = xScale(xVal); 
		xValAdj = spec.xValAdj(xVal); 
		xPosAdj =  xScale(xValAdj); 
		
		if (xPos<0 || xPosAdj >= plotWidth + spec.xMaxBuffer) {main.hide(); return;}
		
		labels.style('display', 'block')
		labels.each(main.update)
		xGuide.style('display','block');
		//xGuideLine.data([[xVal,0],[xVal,100]]).attr('d', getPathD)
		xGuideLine.transition().duration(spec.duration).attr('x1', xPosAdj).attr('x2', xPosAdj) //.attr('y2',plotHeight)
		xGuideRect.transition().duration(spec.duration).attr('x', mouseX - halfWidth)
		xGuideText.transition().duration(spec.duration).attr('x', mouseX).text(spec.xText ? spec.xText(xValAdj) : xValAdj)
		
		if (!arguments.length && spec.postShow) spec.postShow(spec.key, xVal)
	}
	
	main.update = function (d) {
		var label = d3.select(this), circle = label.select('circle'), rect = label.select('rect'), text = label.select('text');
		//d3.select(this).style('display','block')
		
		for(var i=0; i<d.dset.length;i++) {
			var obs = obs=d.dset[i], 
				xSrc = typeof spec.x=='function' ? spec.x(obs) : obs[spec.x];
				
			if (xSrc == xValAdj) {	//if (typeof spec.x!='function') console.log(spec.x);
				var x = xScale(xSrc), //obs[spec.x]),
					ySrc = typeof spec.y=='function' ? spec.y(obs) : obs[spec.y], y = yScale(ySrc),
					textAnchor = typeof spec.textAnchor=='function' ? spec.textAnchor(xVal) : spec.textAnchor;
					
				circle.transition().duration(spec.duration)
					.attr('cx', x)
					.attr('cy', y)
				
				rect.transition().duration(spec.duration)
					.attr('x', textAnchor == 'start' ? x+5 : x-5-spec.width)
					.attr('y', y-10)
				
				text.transition().duration(spec.duration)
					.attr('x', textAnchor == 'start' ? x + 10 : x - 10)
					.attr('y', y + 3)
					.style('text-anchor', textAnchor)
					.text(spec.yText ? spec.yText(obs) : ySrc)	
			}
		}		
	}
	
	main.remove = function (g,d) {
		g.remove();
	}
	
	main.hide = function (key) {
		if (!xScale /*|| !plotNode.offsetParent*/) return;
		labels.style('display', 'none')
		if (!key && spec.postHide) spec.postHide(spec.key)
	}
	
	main.reorder = function () { //console.log(lineLabelsGrp.selectAll('.'+spec.className).node())
		if (!lineLabelsGrp) return;
		var labelSel = lineLabelsGrp.selectAll('.'+spec.className);
		if (!labelSel) return;
		
		$(lineLabelsGrp.node()).append(labelSel.node())
	}
	
	main.specs = function () {console.log(specs)}
	
	return main
}
