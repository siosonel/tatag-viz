/*
Purpose: This is the core engine that renders charts for selected points within 
a dataset. 

Description: 
- Some code and conventions were adopted from http://bost.ocks.org/mike/chart/.
- e4() binds nested data objects onto DOM elements
	- Chart level transitions, such as multiple charts entering or exiting a container div, 
		are not handled here, but instead handled at the viz application level
	- series enter, exits, updates are handled by corresponding pathgen functions
		as called in the e4.update() function below
	- datapoint enter, exits, updates are handled by functions called within 
		the corresponding pathgen function
- the minMax() and zoom functions have been separated to clarify code organization 

Author: Edgar Sioson
Date: 2013-05-24
*/
var esd3 = e4; //map previous name

/****************************************************
	Core function that binds nested data objects, plus 
	calls other functions to draw, update, or remove 
	chart, series, and datapoint element components
******************************************************/

function e4() {
  //these variables are not visible outside of the core function (through closure),
	//unless exposed by an externally accessible function 
	var Dimensions = {"_": { //default chart dimensions
				width: 350, height: 300, margin: {top: 5, right: 20, bottom: 50, left: 50}
			}},      			
				
			legend = {include: [], fxn: function (g,legendArr) {}},  
			toolTip = {elem: [], fxn: function () {}},
		
			wrappers = {}, //array of elements belonging to the same wrapper div
			//zoom variable is declared before returning chart, since it's initialization requires additional functions to be attached to e4fxn
      pathgen = {}, //will be filled in externally through chart.pathgen() 
			scaleXY = {}; //will hold override values for x- and y-scales when zooming in
	
	var minMax = e4_minMax(wrappers, pathgen),
			axes = e4_axes(minMax, scaleXY); //will handle optional chart axes
	
	//the main function that renders the chart
  function main(selection) { 
		if (!selection.length) return; 
		var wrapperId = selection[0].parentNode.id;		
		if (!wrappers[wrapperId]) wrappers[wrapperId] = {};
		
		minMax(); 	
		selection.each(bindRender);
  }
	
	function update(arg1, pts, idStr) {		//update the rendering of charts for a given selection or within a given wrapperId
		if (typeof arg1 == 'string') {
			var wrapperId=arg1, selection = d3.select('#'+wrapperId).selectAll('.'+wrapperId+'_chart');
		}
		else if (Array.isArray(arg1)) {
			var wrapperId = arg1.node().parentNode.id, selection = arg1; 
		}
					
		scaleXY[wrapperId] = arguments.length == 1 || pts === 0 ? {} : pts
		
		//idStr is an additional filter for the ids of charts within a wrapper
		if (idStr) selection = selection.filter(function (data) {if (data.chartId.search(idStr) != -1) return 1})
		
		selection.each(bindRender) 
		if (typeof main.postUpdate == 'function') main.postUpdate()		
		return main;
	}
	
	function bindRender(data) { //bind data on existing or newly appended chart elements
		var wrapperId = this.parentNode.id,
			s = data.dimensions ? data.dimensions : wrapperId in Dimensions ? Dimensions[wrapperId] : Dimensions._ ,
			height = s.height, width = s.width, margin = s.margin;
		
		var	chartId = data.chartId +"_"+ wrapperId,
			chartDiv = d3.select(this),
			millisec = !chartDiv.select('svg').node() ? null : 'duration' in data ? data.duration : 1000;
		
		//set scales, title, svg
		data._e4_wrapperId = wrapperId;
		setDimensions(data, s, chartId, wrapperId)
		setTitle(data, chartDiv)
		var svg = setSVG(data, chartDiv, s)
		
		setClipPath(data, svg, chartId+'_plotClip_'+wrapperId, millisec)	
		var mainGrp = setMainGrp(data, svg, s) //set main wrapper g for translating whole plot area easily
							
		if (millisec) axes.update(data, this, chartId, wrapperId, s);
		else axes.enter(data, this, mainGrp)		
		
		setPlotArea(data, chartId+'_plotArea', mainGrp)	
		
		//bind element data to dataseries 'g'
		setDataseries(data, mainGrp, chartId, wrapperId)
		
		//create legend, would not do anything if there is no 
		//selection in legendArr or if legend.fxn is just function () {}
		if (data.legendFxn) data.legendFxn(mainGrp, data._e4_legendArr)
		else legend.fxn(mainGrp, data._e4_legendArr)
		
		if (data.chartId in minMax.y().zoomed) zoom.setResetBtn(chartDiv)
		if (millisec && typeof data.postUpdate == 'function') data.postUpdate()
	}
	
	function setDimensions(data, s, chartId, wrapperId) {			
		//create scales
		data._e4_yDomain = minMax.get(chartId, wrapperId);
		data._e4_effWidth = s.width - s.margin.left - s.margin.right,
		data._e4_effHeight = s.height - s.margin.top - s.margin.bottom;
		data._e4_xScale = d3.scale.linear().domain(minMax.x(data.chartId)).range([0, data._e4_effWidth]), 							
		data._e4_yScale = d3.scale.linear().domain( data._e4_yDomain ).range([data._e4_effHeight, 0]);
		data._e4_legendArr = [];
		
		//override calculated scales if domain and range is specified by user
		if (wrapperId in scaleXY) {
			var scale = scaleXY[wrapperId], xs =  data._e4_xScale, ys = data._e4_yScale;			
			if ("x" in scale) data._e4_xScale.domain( [ xs.invert(scale.x[0] - s.margin.left) , xs.invert(scale.x[1] - s.margin.left) ] )		
			if ("y" in scale) data._e4_yScale.domain( [ ys.invert(scale.y[1] - s.margin.top), ys.invert(scale.y[0]) ] )			
			if ("x" in scale || "y" in scale) minMax.y().zoomed[data.chartId] = {x: xs.domain(), y: ys.domain()}
		}
	}
	
	function setTitle(data, chartDiv) {
		if (!data.title || typeof data.title != "object") return;
		var titleDiv = chartDiv.select('.'+data.title.className); //console.log(titleDiv)
		if (!titleDiv.node()) titleDiv = chartDiv.append("div").attr("class", data.title.className)
		titleDiv.html(data.title.text)
	}
	
	function setSVG(data, chartDiv, s) {
		var svg = chartDiv.select('svg'), dsvg = 'svg' in data ? data.svg : {};
		
		if (!svg.node()) svg = chartDiv.append("svg").datum(data)
				.attr('id', dsvg.id)
				.attr('class', ('svg' in data) ? data.svg.className : null)
				.attr('height', dsvg.height ? dsvg.height : s.height)
				.attr('width', dsvg.width ? dsvg.width : s.width)
				.attr('viewBox', dsvg.viewBox)
				.attr('preserveAspectRatio', dsvg.preserveAspectRatio)
				.on("mousedown.e4fxn", zoom)
		else 
			svg.transition().duration(data.duration)
				.attr('height', dsvg.height ? dsvg.height : s.height)
				.attr('width', dsvg.width ? dsvg.width : s.width)
		
		return svg
	}
	
	function setClipPath(data, svg, id, duration) {
		if (!data.svg || !data.svg.plotclip) return;
		
		var clip = typeof duration == 'number' 
			? svg.select('#'+id).select('rect').transition().duration(duration)							
			: svg.append('defs').append('clipPath').attr('id', id).append('rect')
			
		clip.attr('x', data.x)
		clip.attr('y', data.y)
		clip.attr("width", data._e4_effWidth)
		clip.attr("height", data._e4_effHeight)
	}
	
	function setMainGrp(data, svg, s) {
		var cls = data.mainGrp && data.mainGrp.className ? data.mainGrp.className : 'chart_mainGrp',
			id = data.mainGrp && data.mainGrp.id ? data.mainGrp.id : null,
			transform = data.mainGrp && 'transform' in data.mainGrp ? data.mainGrp.transform : "translate(" + s.margin.left + "," + s.margin.top + ")";
	
		var g = svg.select("."+cls)		
		if (!g.node()) g = svg.append("g").attr("class", cls).attr('id', id).attr('transform', transform);
		else g.transition().duration(data.duration).attr("transform", transform);		
			
		return g;
	}
	
	function setPlotArea(data, id, g) { //add rectangle, a.k.a. stylable background, to capture clicks within plot area
		 var plotArea = g.select('#'+id);
		 if (!plotArea.node()) g.append("rect").attr('id',id).attr("class", data.plotArea.className)
		
		plotArea.attr('x', data.x)
			.attr('y', data.y)
			.attr("width", data._e4_effWidth)
			.attr("height", data._e4_effHeight)
	}
	
	function setDataseries(data, g, chartId, wrapperId) {
		//bind new element data to dataseries 'g'
		var s = data.seriesWrapper;
		
		var dataSeriesWrapper = g.select('.dataSeries')	
		if (!dataSeriesWrapper.node()) dataSeriesWrapper = g.append("g")
			.attr('id', s && s.id ? s.id : null)
			.attr("class", s && s.className ? s.className : 'dataSeries')
			.attr("transform", s && s.transform ? s.transform : null)
			.attr('clip-path', data.svg.plotclip ? 'url(#'+chartId+'_plotClip_'+ wrapperId +')' : '')
			.selectAll("g") 
		else if (s && s.transform) g.select('.dataSeries').transition().duration(data.duration).attr("transform", s.transform)
		
		var dataSeries = dataSeriesWrapper.selectAll('g').data(data.elems, data.bindkey);

		//pass removed elements to pathgen function for removal or otherwise 
		data._e4_removedIds = []			
		dataSeries.exit().each(seriesExit)
		
		//add new alements and pass to pathgen function
		dataSeries.enter().append("g")
			.attr("id", function (d) {return d.id +"_"+wrapperId })
			.attr('class', gSeriesClass)
			.each(seriesEnter)
		
		//update remaining series
		dataSeries.each(seriesUpdate)
	}
	
	function gSeriesClass(d) {
		return typeof d.gClass=='function' ? d.gClass(d) : d.gClass;
	}
		
	
	function seriesEnter(d) { 
		if (!d.type || !pathgen[d.type]) return;
	
		var g = d3.select(this), pdata = this.parentNode.__data__;
		
		//Note that different wrapperIds will share the same scale functions
		//for the same data series; has to recalc domain & range on update
		if (!("xScale" in d)) d.xScale = pdata._e4_xScale;	
		else d.xScale.domain(minMax.x()).range([0, pdata._e4_effWidth])
		
		if (!("yScale" in d)) d.yScale = pdata._e4_yScale;
		else d.yScale.domain( pdata._e4_yDomain ).range([pdata._e4_effHeight, 0])
		
		if (minMax.excluded(d.id)) g.style("display", "none");																
		pathgen[d.type].draw(g, d, pdata._e4_wrapperId)
		d.fxn = pathgen[d.type];

		if ("dset" in d && typeof d.className == 'string' && d.className.search('pt_hidden') == -1 
			&& (!('_e4_removedIds' in pdata) || pdata._e4_removedIds.indexOf(d.id) == -1)) pdata._e4_legendArr.push(d); 
	}
	
	function seriesExit(d) {
		if (!d.type || !pathgen[d.type]) return;
		
		var pdata = this.parentNode.__data__; //console.log(pdata)
		
		pathgen[d.type].elemExit(d3.select(this), d, pdata._e4_wrapperId)
		if (pdata._e4_removedIds) pdata._e4_removedIds.push(d.id); //legendArr = main.disjoin(legendArr, [d]); 
	}
	
	function seriesUpdate (d) { //if (!dObj.type) return; console.log(dObj.dset[0]);
		if (!d.type || !pathgen[d.type]) return;
		
		var g = d3.select(this), //d3.select(document.getElementById(dObj.id+'_'+wrapperId)), 
			pdata = this.parentNode.__data__;
						
		d.xScale = pdata._e4_xScale;				
		d.yScale = pdata._e4_yScale;
		if (!('fxn' in d)) d.fxn = pathgen[d.type]; 				
							
		if (minMax.excluded(d.id)) g.style("display","none")
		else {							
			g.style("display","block")				
			pathgen[d.type].update(g,d)					
		}
		
		if ("dset" in d && d.dset.length && typeof d.className == 'string' 
			&& d.className.search('pt_hidden') == -1 
			&& pdata._e4_removedIds.indexOf(d.id) == -1) pdata._e4_legendArr.push(d)
	}
		
	//make update function available externally
	main.update = update
	
	main.minMaxCalc = function (xRange, yRange) {
		minMax(xRange,yRange);
		return main;
	}
	
	main.minMaxGrp = function (obj) { 
		if (!arguments.length) return minMax.grp();
		minMax.grp(obj)		
		return main;
	}
		
	//get or set the closured Dimensions object
	main.styles = function(wrapperId, obj) {
    if (!arguments.length) return Dimensions;
    if (!arguments.length == 1) return Dimensions[wrapperId];
		
		if (!Dimensions.hasOwnProperty(wrapperId)) Dimensions[wrapperId] = {}		
		for(var prop in obj) (Dimensions[wrapperId])[prop] = obj[prop];
		
		//fill in any missing style property for wrapperId
		for(var prop in Dimensions._) {
			if (!Dimensions[wrapperId].hasOwnProperty(prop)) (Dimensions[wrapperId])[prop] = Dimensions._[prop];
		}
		
		return main;
  }
	
	//get or set wrapper dimensions
	main.dimensions = function (obj) {
		if (!arguments.length) return Dimensions;
		for (var prop in obj) Dimensions[prop] = obj[prop]
		return main;
	}
	
	//get or set the closured exclude array
	main.exclude = function(str, not) {
		if (!arguments.length) return minMax.exclude();
		minMax.exclude(str,not)		
		return main;
	}
	
	main.excluded = minMax.excluded; //function to find out if an lement is excluded from rendering
	
  main.x = function(_) {
    if (!arguments.length) return xValue;
    xValue = _;
    return main;
  }

  main.y = function(_) {
    if (!arguments.length) return yValue;
    yValue = _;
    return main;
  }
	
	//get or set the toolTip 
	main.toolTip = function (arg) {
		if (!arguments.length) return toolTip.fxn;
		
		if ("elem" in arg && "fxn" in arg) {
			toolTip = arg; //set what fxn to use			
			main.toolTipSet( arg.fxn ) //activate listening on mouseover
		}	
		
		return main;
	}
	
	//activate tooltip fxn on mouseover
	main.toolTipSet = function (fxn) { 
		toolTip.elem.forEach( function (d) {
			d3.select(d).on("mouseover", fxn).on("mousemove.tooltip", fxn)
		})
	}
	
	//get or set the closured legend object, fxn
	main.legend = function (obj) {
		if (!arguments.length) return legend;
    for(var prop in obj) {
			legend[prop] = obj[prop];
		}
		
		return main;
	}
	
	main.wrappers = function () {
		return wrappers;
	}
	
	main.preppedFxns = function () {
		return preppedFxns;
	}
	
	main.zoomOk = function (d, op) {
		if (!arguments.length) return zoom.ok();
		zoom.ok(d, op);	
		return main;
	}
	
	main.pathgen = function (obj) {
		if (!arguments.length) return pathgen;
		
		for(var prop in obj) pathgen[prop] = obj[prop];		
		return main;
	}
	
	main.disjoin = function(a, b) {
		return $.grep(a, function($e) { return $.inArray($e, b) == -1; });
	}
	
	var zoom = e4_zoomPlot(minMax, main)
	main.zoom = zoom
	
  return main;
}

