

function e4_arrow() {
	//default layout
	var wrapperDiv = ''; //has to be valid d3 selector notation or object(s)
	var rowKey = 0; //could use string property name instead of index#, depending on processed data structure
	var valKey = 0; //could use string property name instead of index#, depending on processed data structure
	
	var rankMin = 0, rankMax = 9; //zero-index ranks
	var idSearchVal; //if provided, the rankMin and rankMax will be determined based on displaying the desired cell value
	var idSearchMin; //the min rank to use when idSearchVal has to be displayed
	var preSearchMinCells;
	
	var layout = { //defaults
		rowHeight: 22, colInterval: 300, //colInterval is x-position of each column of rect's
		colTitleX: 45,
		rectHeight: 20, rectWidth: 250, 
		textX: 5, textY: 15, connectorY: 10, //relative to rect x,y position
		rankMinOffset: -5, rankMaxOffset: 5,
		hiddenY: 2000
	}
	
	//these functions may be overwritten by the instantiator of e4_arrow
	//using the corresponding main.* function(fxn)
	var chartTitle = function () {}
	var rectFill = function (d,i) {return 'rgb(200,200,200)'}
	var strokeColor = function (d,i) {return 'rgb(200,200,200)'}
	var strokeWidth = function (d,i) {return '1px';}
	var fontWeight = function (d,i) {return idSearchVal && idSearchVal==d[rowKey] ? 700 : 400}
	var cellTextVal = function (d,i) {return i+' '+d[rowKey]}
	var textID = function (d,i) {return 'arrowText_'+ d[rowKey] +'_'+i;}
	var rectID = function (d,i) {return 'arrowRect_'+ d[rowKey] +'_'+ i;}
	var lineID = function (d,i) {return 'arrowLine_'+ d[rowKey] +'_'+ i;}
	
	var projectedDimen = getProjectedDimen();
	
	var wrapperData = [
		getWrapperData('arrow_0') //, getWrapperData('cause_1'), getWrapperData('risk_0'), getWrapperData('risk_1')
	];
	
	var colTitles = {
		type: 'arrow',
		dset: [], 
		bindKey: bindKeyFxn,
		
		id: 'arrowText_title',
		className: 'arrowRow',
		
		transform: 'translate(0,0)',
		cellText: {
			id: colTitleID, bindKey: colTitleID, 
			className: 'arrowColTitle',
			val: function (d,i) {return 'Week #'+d}, 
			x: colTitleX, y: -layout.textY + 5, delay: 5000
		},
		duration: 2000
	}
	
	var numCols = 0; 
	var sortedByRank = [[], [], [], [], [], []]; //filtered list of ranked causes or risks to those that appear within rankMin-rankMax within any displayed year
	var fullSortedByRank = [[], [], [], [], [], []]; //unfiltered list so that the correct rank# can be displayed (not possible using the filtered ranked list for those causes or risks that are ranked beyond rankMax)
	var sorterFxn = getSorterFxn();
	
	var hoveredG; //tracks hovered element for highlighting connectors and rects
	var toolTipFxn = function () {}
	var clickedCauses = []
	
	$(document).ready(function () {$(wrapperDiv).mouseover(hoverFxn)});
	
	//expects data to be array of connectable 'row' data arrays
	function main(data) {
		if (!wrapperDiv) {console.log("Null value for wrapperDiv."); return;}
		
		mainData = data;		
		setSeriesElems();
		draw();
	}
	
	function getWrapperData(chartId) {
		return {
			chartId: chartId,				
			elems: [],		
			bindkey: bindKeyFxn,
			//x: 0, y: 0,
			title: {
				className: "chartTitle",
				text: chartTitle, 
			},
			plotArea: {
				className: 'plotArea'
			},
			svg: {
				className: 'arrows_svg',				
				//viewBox: '0 0 500 400',
				//preserveAspectRatio: 'xMinYMin meet',
				height: '100%',
				width: '100%' //,
				//width: function () {return (numCols*100)+'%'}
			},
			mainGrp: {
				transform: "translate(50,20)"
			}
		}
	}
	
	function setSeriesElems() {
		idSearchMin = 9999999; //value to ensure idSearchMin will have value, if applicable
		preSearchMinCells = 0; //the maxi
		
		//create a cause or risk sort order for each column in mainCols
		for(var i=0; i< numCols; i++) {
			setSortedByRank(mainData, i);
		}
		
		//override rankMin, rankMax value if an idSearchVal is specified
		if (idSearchVal) {
			rankMax = idSearchMin + rankMax - rankMin;
			rankMin = idSearchMin - 1;
		}
		
		//drop any cause or risk elems for which values in the selected years are not within the user-selected rank range
		mainData = mainData.filter(filterByRankRange);
		
		//filter each sortedByRank sub-array to just the causes or risks that are within the user-selected rank range
		for(i=0; i< numCols; i++) {
			sortedByRank[i] = fullSortedByRank[i].filter(filterSortedByRank, mainData) //tempElems='this' object by filter function
		}
		
		//fill the wrapper data series
		colTitles.dset = colTitlesArr;
		wrapperData[0].elems = [colTitles]
		mainData.map(setArrowElem);
	}

		
	function setSortedByRank(elems, i) {
		elems.sort(sorterFxn.colKey(i));
		fullSortedByRank[i]=[]
		for(var j=0; j<elems.length; j++) {
			if (elems[j][i]) {
				fullSortedByRank[i].push(elems[j][i][rowKey])
				if (idSearchVal && elems[j][i][rowKey]==idSearchVal) {
					var tempMin = fullSortedByRank[i].length-1;
					if (tempMin < idSearchMin) idSearchMin = tempMin;
				}
			}
		}
	}
	
	function getSorterFxn() { //this function is instantiated in the sorterFxn variable declaration at the top
		var colKey=0, valKey=0;
		
		function main(a,b) {
			if (!a[colKey] || !b[colKey]) return -1;
			return b[colKey][valKey] - a[colKey][valKey] 
		}
		
		main.colKey = function (indexOrKeyName) {colKey=indexOrKeyName; return main;}
		main.valKey = function (indexOrKeyName) {valKey=indexOrKeyName; return main;}
		return main;
	}
	
	function filterByRankRange(d) { 
		var inRange = 0, sortOrder=-1;
		
		for(var i=0; i< numCols; i++) { 
			if (d[i]) {				
				sortOrder = fullSortedByRank[i].indexOf(d[i][rowKey]);
				if (sortOrder >= rankMin && sortOrder <= rankMax) {
					inRange = true; break;
				}
			}
		}
		
		return inRange;
	}
	
	function filterSortedByRank(crId) {
		var elems = this, toBeDisplayed=0;
	
		for(var i=0; i < elems.length; i++) {
			for(var y=0; y< numCols; y++) {
				if (elems[i][y] && elems[i][y][rowKey]==crId) {
					toBeDisplayed=1; 
					if (i < idSearchMin && preSearchMinCells < i) preSearchMinCells = i;					
					break;
				}
			}
			
			if (toBeDisplayed) break;
		}
		
		if (!toBeDisplayed && clickedCauses.indexOf(crId)!=-1) clickedCauses = $.disjoin(clickedCauses, [crId])
		
		return toBeDisplayed;
	}
	
	function colTitleID(d,i) {
		return 'arrowText_title_'+d
	}
	
	function colTitleX(d,i) {
		return rectX(d,i) + layout.colTitleX
	}
	
	function setArrowElem(rowData) {
		wrapperData[0].elems.push({
			type: 'arrow',
			dset: rowData,
			bindkey: bindKeyFxn,
			
			id: 'arrowRow_'+ rowData[0][rowKey],
			className: 'arrowRow',
			
			transform: 'translate(0,0)',
			cellRect: {
				id: rectID, bindKey: rectID,
				className: 'cellRect',
				height: layout.rectHeight, width: layout.rectWidth, 
				x: rectX, y: rectY, 
				fill: rectFill, stroke: strokeColor
			},
			cellText: {
				id: textID, bindKey: textID,
				className: 'arrowCellText',
				val: textVal, 
				x: textX, y: textY, 
				anchor: 'start', delay: textDelay,
				fontWeight: fontWeight
			},
			connector: {
				id: lineID, bindKey: lineID,
				className: 'cellConnector',
				x1: connectorX1, x2: connectorX2, y1: connectorY1, y2: connectorY2, 
				stroke: strokeColor, opacity: lineOpacity,
				strokeDashArray: dashArray,
				display: connectorDisplay
			},
			hiddenY: layout.hiddenY,
			duration: 3000, 
		})
	}
	
	function bindKeyFxn(d) {
		return d.id
	}
	
	function rectX(d,i) {		
		var colInterval = typeof layout.colInterval=='function' ? layout.colInterval() : layout.colInterval;	
		return i*colInterval
	}
	
	function rectY(d,i) {
		if (i>numCols-1) return layout.hiddenY;
		
		var rank = sortedByRank[i].indexOf(d[rowKey]), 
			fullRank = fullSortedByRank[i].indexOf(d[rowKey]),
			offset=0;
		
		if (rank==-1) return layout.hiddenY;
		
		if (fullRank <= rankMin) offset = layout.rankMinOffset;		
		if (fullRank > rankMax) offset = layout.rankMaxOffset;
		
		return rank*layout.rowHeight + offset;
	}
	
	//not being used
	function scaledRank(d,i,rankMax) {
		var r = fullSortedByRank[i].indexOf(d[rowKey]);
		return layout.rowHeight*(rankMax+1) + projectedDimen[r-rankMax].y + layout.rankMaxOffset;
	}
	
	//not being used
	function getProjectedDimen() {
		var arr=[], heightTotal=0, heightCurr=0;
		
		for(var i=0; i<200; i++) {
			heightCurr = layout.rowHeight*(200-i)/1000;
			arr.push({ht: heightCurr, y: heightTotal})
			heightTotal += heightCurr;			
		}
		
		return arr
	}
	
	function textVal(d,i) {
		var rank = fullSortedByRank[i].indexOf(d[rowKey]);
		return cellTextVal(rank,d,i);
	}
	
	function textX(d,i) {
		return rectX(d,i) + layout.textX
	}
	
	function textY(d,i) {
		return rectY(d,i) + layout.textY;
	}
	
	function textDelay(d,i) {
		return 3000 + sortedByRank[i].indexOf(d[rowKey])*250
	}
	
	function connectorX1(d,i) {
		var rectWidth = typeof layout.rectWidth=='function' ? layout.rectWidth() : layout.rectWidth;
		var colInterval = typeof layout.colInterval=='function' ? layout.colInterval() : layout.colInterval;		
		return rectWidth + i*colInterval;
	}
	
	function connectorX2(d,i) {
		var colInterval = typeof layout.colInterval=='function' ? layout.colInterval() : layout.colInterval;
		return (i+1)*colInterval;
	}
	
	function connectorY1(d,i) {
		if (i>=numCols-1) i=numCols-1; // return layout.hiddenY;
		return rectY(d,i) + layout.connectorY;
	}
	
	function connectorY2(d,i) {
		if (i>=numCols-1) i=numCols-2; // return layout.hiddenY;
		return rectY(d,i+1) + layout.connectorY
	}
	
	function lineOpacity(d, i) {
		return i>=numCols-1 ? 0 : 1;
	}
	
	function dashArray(d, i) {
		return fullSortedByRank[i].indexOf(d[rowKey]) >= fullSortedByRank[i+1].indexOf(d[rowKey]) 
			? null : '10,5';
	}
	
	function connectorDisplay(d, i) {
		return d && fullSortedByRank[i+1].indexOf(d[rowKey]) != -1 ? '' : 'none'
	}
	
	function draw() { //console.log(wrapperData[0].elems)
		var arrows = d3.select(wrapperDiv)
			.selectAll('.arrow_chart')
			.data(wrapperData, function (d) { return d.chartId })
		
		arrows.enter().append('div')
			.attr('class', 'arrow_chart')
			.call(app.mgr)
			
		arrows.call(app.mgr.update)		
	}
	
	function hoverFxn(e) {	
		if (hoveredG && clickedCauses.indexOf(hoveredG.firstChild.__data__[0])==-1) {		
			var g = d3.select(hoveredG).style('stroke-width','');
			g.selectAll('rect').style('fill', rectFill)
		}
	
		if (['rect','text','line'].indexOf(e.target.tagName)==-1 || e.target.parentNode.className.baseVal!='arrowRow') {
			hoveredG = null; toolTipFxn('hide'); return;
		}
		
		hoveredG = e.target.parentNode;
		var g = d3.select(hoveredG).style('stroke-width','3px');
		g.selectAll('rect').style('fill', '#fff')
		toolTipFxn(e)
	}
	
	function clickFxn(e) {
		if (['rect','text','line'].indexOf(e.target.tagName)==-1) return;
		if (typeof e.target.className == 'string') return;
		if (e.target.className.baseVal == 'plotArea') return; //console.log(e.target.className.baseVal )
		
		var causeID = e.target.__data__[0], p = d3.select(e.target.parentNode);
	
		if (clickedCauses.indexOf(causeID)==-1) clickedCauses.push(causeID)			
		else clickedCauses = $.disjoin(clickedCauses,[causeID])
		
		if (clickedCauses.indexOf(causeID)!=-1) {
			p.selectAll('rect').style('fill', '#fff')
			p.style('stroke-width','3px');
		}
		else {
			p.selectAll('rect').style('fill', rectFill)
			p.style('stroke-width','');
		} 
	}
	
	main.docReady = function () {
		$(wrapperDiv).mouseover(hoverFxn).click(clickFxn)		
		colTitles.dset = colTitlesArr; //console.log(colTitles)
	}
	
	main.wrapperDiv = function (arg) {
		if (!arguments.length) return wrapperDiv
		wrapperDiv = arg
		return main
	}
	
	main.numCols = function (num) { 
		if (!arguments.length) return numCols
		numCols = num;
		return main
	}
	
	main.rankLength = function () {
		return sortedByRank[0].length
	}
	
	main.rowKey = function (strOrInt) {
		if (!arguments.length) return rowKey
		rowKey = strOrInt
		return main
	}
	
	main.valKey = function (strOrInt) {
		if (!arguments.length) return valKey;
		valKey = strOrInt;
		sorterFxn.valKey(strOrInt);
		return main;
	}
	
	main.rankMinMax = function (min, max) {
		if (!arguments.length) return [rankMin, rankMax]
		rankMin = min-1; rankMax = max;
		return main
	}
	
	main.layout = function (obj) {
		if (!arguments.length) return layout;
		for(var prop in obj) layout[prop] = obj[prop]
		return main
	}
	
	main.colTitlesArr = function (arr) {
		if (!arguments.length) return colTitlesArr;
		colTitlesArr = arr;
		return main;
	}
	
	main.chartTitle = function (fxn) {
		if (!arguments.length) return chartTitle;
		if (typeof fxn=='function') {
			if (wrapperData.length) wrapperData[0].title.text = fxn;
			else chartTitle = fxn;
		}
		
		return main;
	}
	
	main.rectID = function (fxn) {
		if (!arguments.length) return rectID;
		if (typeof fxn=='function') rectID = fxn;
		return main;
	}
	
	main.textID = function (fxn) {
		if (!arguments.length) return textID;
		if (typeof fxn=='function') textID = fxn;
		return main;
	}
	
	main.lineID = function (fxn) {
		if (!arguments.length) return lineID;
		if (typeof fxn=='function') lineID = fxn;
		return main;
	}
	
	main.rectFill = function (fxn) {
		if (!arguments.length) return rectFill;
		if (typeof fxn=='function') rectFill = fxn;
		return main;
	}
	
	main.strokeColor = function (fxn) {
		if (!arguments.length) return strokeColor;
		if (typeof fxn=='function') strokeColor = fxn;
		return main;
	}
	
	main.strokeWidth = function (fxn) {
		if (!arguments.length) return strokeWidth;
		if (typeof fxn=='function') strokeWidth = fxn;
		return main;
	}
	
	main.cellTextVal = function (fxn) {
		if (!arguments.length) return cellTextVal;
		if (typeof fxn=='function') cellTextVal = fxn;
		return main;
	}
	
	main.fontWeight = function (fxn) {
		if (!arguments.length) return fontWeight;
		if (typeof fxn=='function') fontWeight = fxn;
		return main;
	}
	
	main.idSearchVal = function (num) {
		if (!arguments.length) return idSearchVal;
		idSearchVal = num;
		return main;
	}
	
	main.toolTipFxn = function (fxn) {
		if (!arguments.length) return toolTipInfo;
		toolTipFxn = fxn
		return main
	}
	
	return main
}