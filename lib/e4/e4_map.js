/*
Purpose: render group of map shapes
Author: Edgar Sioson
Date: 2014-09-19
*/

if (!e4_pathgen) var e4_pathgen = {}

e4_pathgen.nestedMap = {			
	draw: function mapGrpDraw(g, d, wrapperId, update) {
		g.attr('transform', d.transform)
			.attr('id', d.id)
			.attr('class', d.className)
			.selectAll("."+d.className)
			.data(d.dset, d.bindKey) 			
		.enter().append('g')
			.attr('id', d.g.id)
			.attr('class', d.g.className)
			.selectAll("."+d.path.className)
			.data(d.g.dset, d.g.bindKey)
		.enter().insert("path", ".graticule")
			.attr('id', d.path.id)
			.attr("class", d.path.className)      
			.attr("d", d.path.d)
			.style("fill", d.path.fill)
			//.style('stroke', d.path.stroke)
			.style('stroke-width', d.path.strokeWidth)
	},			
	minMax: function mapGrpMinMax(xMinMax, yMinMax, d) {
		return yMinMax
	},	
	update: function mapGrpUpdate(g,d) {
		g.selectAll(d.updateSelector)//.transition().delay(d.duration)
			//.attr("d", d.path)
			.style("fill", d.path.fill)
			//.style('stroke', d.path.stroke)
			//.style('stroke-width', d.path.strokeWidth)
	},	
	exit: function mapGrpExit(g, d, wrapperId) {
		g.transition().duration(d.duration).style('opacity', 0).remove();		
	}
}



function e4_colorScale(spec) {
	var legendColors, legendColorsReversed, currColors;
	var legend_bars, legend_axes, legend_labels;
	
	
	function main(mainGrp, legendArr) {
		var g = mainGrp.select('#'+spec.id)
		if (!g.node()) draw(mainGrp, spec)
		else update([0,100], 0)
	}
	
	function draw(mainGrp, d) {
		legendColors = setColors(d, d.colors); 
		legendColorsReversed = setColors(d, d.colorsReversed);
		currColors = legendColors;
		
		var g = mainGrp.append('g')
			.attr('id', d.id)
			.attr('transform', d.transform);
		
		if (d.background) g.append('rect') //for background color
			.attr("x", d.background.x)
			.attr("y", d.background.y)
			.attr("width", d.background.width)
			.attr("height", d.background.height)
			.attr('fill', d.background.fill);
		
		g.append('text')
			.attr('x',d.label.x)
			.attr('y',d.label.y)
			.attr('class', d.label.className)
			.text(d.label.text)
		
		legend_bars = g.append("g")
			.selectAll(d.bars.className)
			.data(d3.range(d.width))
		.enter().append('rect')
			.attr("class", d.bars.className)
			.attr('y', '0px')
			.attr('width', '1px')
			.attr('height', '15px')
			.attr('stroke', 'none')
			.attr('x', barX)
			.attr('fill', barFill);
		
		legend_scales = d3.scale.linear()
			.domain([0,1])
			.range([0,d.width]);
		
		legend_axes = d3.svg.axis()
			.scale(legend_scales)
			.ticks(5)
			.tickFormat(function (d) { return d.toPrecision(2) })
			.orient('bottom');
		
		legend_labels = g.append('g')
			.attr('transform', 'translate(0,20)')
			.attr('class', 'axis')
			.attr('id', 'legend_labels')
			
		legend_labels.call(legend_axes); console.log(d.bins)
		
		if (d.bins) g.selectAll('.'+ d.bins.className)
			.data(d.bins.arr).enter().append('g').attr('class', d.bins.className)
			.each(binFxn)
	}
	
	function update(minMax, isReversedScale) {
		currColors = isReversedScale ? legendColors : legendColorsReversed;
	
		legend_bars.attr('fill', barFill);
		legend_scales.domain(minMax)
		legend_axes.scale(legend_scales)
		legend_labels.transition().duration(2000).call(legend_axes)
		
		if (spec.bins) {
			var bins = d3.select('#'+spec.id).selectAll('.'+ spec.bins.className).data(spec.bins.arr)
			
			bins.exit().remove()			
			bins.enter().append('g').attr('class', spec.bins.className).each(binFxn)
			bins.each(binFxn)
		}
	}
	
	function setColors(d, colors) {
		return d3.scale.linear() 
				.domain(d3.range(0, d.width, d.width/d.colorSteps))
				.range(colors)
				.clamp(true);
	}	
	
	function barX(d) {
		return d + 'px'; 
	}
	
	function barFill(d) { 
		return currColors(d); 
	}
	
	function binFxn(d) { 
		if (!this.firstChild) {
			var g = d3.select(this), rect = g.append('rect'), text = g.append('text');
		}
		else {
			var g = d3.select(this), rect = g.select('#'+d.rectId), text = g.select('#'+d.textId);
			text.selectAll('tspan').remove();
		}
		
		rect.attr('id', d.rectId)
			.attr("x", d.rectX)
			.attr("y", d.rectY)
			.attr("width", d.width)
			.attr("height", d.height)
			.attr('fill', d.fill)
		
		var tspans = text.attr('id', d.textId)
			.style('text-anchor', d.textAnchor)
			.data(d.tspans)		
		
		for(var i=0; i < d.tspans.length; i++) {		
			text.append('tspan')
				.attr("x", d.tspans[i].x)
				.attr("y", d.tspans[i].y)
				.text(d.tspans[i].text)
		}
	}
	
	return main
}




function e4_zoomGrab(spec) {
	var scrollScale = 1, zTimer;
	
 	var zoomXY = [], xOffset=0, yOffset=0, ratio=spec.scale, 
			cx=ratio*spec.width/2 - xOffset, cy=ratio*spec.height/2 - yOffset;
	
	var clickHandler = function () {}
	
	var behavior = d3.behavior.zoom()
		.translate([spec.left, spec.top])
		.scale(spec.scale)
		.scaleExtent(spec.scaleExtent)
		.on("zoom", zoomD3dispatch)
	
	function main (rect, params) {
		if (!rect) reset()
		else {			
			if (Math.round(rect) == 1) { //console.log('reset')
				reset(); return;
			}				
			else if (arguments.length==2) { //console.log(params.cx+','+params.cy+' '+cx+','+cy+' '+rect+':'+ratio)
				if (rect == ratio) return; //another event will be triggered by drag function below
				cx = (params.cx - xOffset)/ratio
				cy = (params.cy - yOffset)/ratio; //console.log(cx+','+cy+' --- '+ rect +' '+ params.ratio)
				
				var prevRatio = Math.round(ratio)
				ratio = rect; var tempX=xOffset, tempY=yOffset
				xOffset = params.xOffset //+ (prevRatio==1 ? 0 : 0)
				yOffset = params.yOffset //+ (prevRatio==1 ? -140*ratio : 0)
			}
			else if (typeof rect == 'number') { //console.log(cx+','+cy) //console.log('slider') //value from zoom slider					
				if (ratio == rect) return; var tempX=xOffset, tempY=yOffset; //console.log(cx+','+cy+' --- '+ratio+' '+rect); 
				ratio = rect				
				xOffset = -(ratio*cx - spec.width/2)
				yOffset = -(ratio*cy - spec.height/2);	//console.log(cx+','+cy+'---'+tempX+','+tempY+' vs slider '+xOffset+','+yOffset)		
			}
			else { //console.log(ratio) //console.log('area-select') //value from cursor selected area
				var w = rect.x[1] - rect.x[0],
					h = rect.y[1] - rect.y[0],
					xRatio = spec.width/w,
					yRatio = spec.height/h;
				
				ratio = Math.min(xRatio, yRatio); //maintain aspect ratio				
				xOffset = -ratio*(rect.x[0] - xOffset);
				yOffset = -ratio*(rect.y[0] - yOffset);		
				cx = rect.x[0] + w/2; 
				cy = rect.y[0] + h/2;
			}

			d3.select(spec.selectors.target)
				.transition().duration(1000)			
				.attr('transform', 'translate('+ xOffset +','+ yOffset +')scale('+ ratio +')')						
			
			behavior.scale(ratio).translate([xOffset,yOffset]);
			scrollScale = ratio
			/*$('#svg_global_mort_map, .country, #mainGrp_global_mort_map').css({
				'cursor': 'url(https://mail.google.com/mail/images/2/openhand.cur)'
			})*/
		}
		
		app.params().zoom = main.getParams()		
	}
	
	function grab() {
		zoomXY = d3.mouse(this)
		ratio = params.map.scale
		
		d3.select(spec.selectors.capture)
			.on('mousedown.zoomGrab', null)
			.on('mousemove.zoomGrab', drag)
			.on('mouseup', release)
		
		$(spec.selectors.grab).css({
			'cursor': 'url(https://mail.google.com/mail/images/2/closedhand.cur)'
		})
	}
	
	function drag() {
		var tempXY = d3.mouse(this)
			dx = zoomXY[0] - tempXY[0], 
			dy = zoomXY[1] - tempXY[1];
		
		d3.select(spec.selectors.target)
			.attr('transform', 
				'translate('+ (xOffset-dx) +','+ (yOffset-dy) +')'
				+ 'scale('+ ratio +')')
	}
	
	function release() {
		d3.select(spec.selectors.capture)
			.on('mousemove.zoomGrab', null)
			.on('mousedown.zoomGrab', grab)
		
		var tempXY = d3.mouse(this)
			dx = zoomXY[0] - tempXY[0], 
			dy = zoomXY[1] - tempXY[1];
		
		xOffset = xOffset - dx
		yOffset = yOffset - dy			
		cx= cx + dx/ratio
		cy= cy + dy/ratio
		
		$(spec.selectors.grab).css({
			'cursor': '' //'url(https://mail.google.com/mail/images/2/openhand.cur)'
		})
		
		app.params().zoom = main.getParams()
	}
	
	function reset() {		
		//$(this).remove(); 
		cx = spec.width/2; 
		cy = spec.height/2;
		xOffset = spec.mapLeft;
		yOffset = spec.mapTop;
		ratio = 1;
		
		d3.select(spec.selectors.target)
			.transition().duration(1000)
			.attr('transform', 'translate('+xOffset+','+yOffset+')scale(1)'); 
		
		d3.select(spec.selectors.capture)
			.on('mousemove.zoomGrab', null)
			.on('mousedown.zoomGrab', grab)
		
		$(spec.selectors.grab).css({
			cursor: ''
		})
		
		//$('#zoomSlider').slider('option','value',1)
		//$('#zoomLevel').html('1x')
		
		behavior.scale(ratio).translate([xOffset,yOffset]);
		app.params().zoom = main.getParams()
	}
	
	function zoomD3dispatch() {
		var e = d3.event.sourceEvent,
			cls = e.target.className.baseVal;
		
		if (e.type != 'wheel' && e.type != 'mousewheel') {
			if (cls=='country' || cls=='subnational') {
				clickHandler(d3.event.sourceEvent); return;
			}
		} 
		
		var s = d3.event.scale, t = d3.event.translate, xy = d3.mouse(this); //console.log(s+' '+xy+' '+ t[0] +','+ t[1])
			
		if (s == scrollScale) return; //zoom rect or drag will be handled differently
		else scrollScale = s; //console.log(s+' '+cls)
		
		main(s, {cx: xy[0], cy: xy[1], xOffset: t[0], yOffset: t[1]})
	}
	
	main.setParams = function (params) { //console.log(params)
		if ('ratio' in params) ratio = params.ratio;
		//if ('zoomXY' in params) zoomXY = typeof zoomXY== 'string' ? zoomXY .split(',') : zoomXY
		if ('xOffset' in params) xOffset = params.xOffset
		if ('yOffset' in params) yOffset = params.yOffset
		if ('cx' in params) cx = params.cx
		if ('cy' in params) cy = params.cy
		behavior.scale(ratio).translate([xOffset,yOffset]);
		return main;
	}
	
	main.getParams = function (type) {
		if (!arguments.length || type) return {
			ratio: ratio, xOffset: xOffset, yOffset: yOffset, cx: cx, cy: cy
		}
		else return 'ratio='+ ratio
			//+ '&zoomXY'+ zoomXY.join(",")
			+ '&xOffset='+ xOffset
			+ '&yOffset='+ yOffset
			+ '&cx=' + cx
			+ '&cy=' + cy
	}
	
	main.ratio = function () {return ratio}
	
	var initialized = 0
	main.init = function () { //console.log(spec.selectors.capture)
		if (initialized) return;
		d3.select(spec.selectors.capture).on('mousedown.zoomGrab', grab)
		d3.selectAll(spec.selectors.svg).call(behavior)
		intialized = 1;
	}
	
	main.zoomD3dispatch = zoomD3dispatch
	
	main.clickHandler = function (fxn) {
		if (!arguments.length) return clickHandler
		clickHandler = fxn
		return main
	}
	
	return main
}
