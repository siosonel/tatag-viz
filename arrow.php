<?php 

require_once "config.php";
$currWeek = date('W');
$prevWeek = $currWeek - 1;

?><!DOCTYPE html>
<html>
<head>
	<title>VizArrow</title>	
	<link rel="icon" type="image/png" href="/ui/css/logo5.png" />	
	
	<meta name="viewport" content="width=device-width, initial-scale=1" />
	<meta http-equiv="Content-Type" content="text/html;charset=utf-8"/>
	
	<link rel="stylesheet" href="css/normalize.css">
	<link type='text/css' rel='stylesheet' href="css/ranks.css" />
	<link rel="stylesheet" href="/common2/lib/foundation-5.3.3/css/foundation.min.css">
	<link rel="stylesheet" href="/common2/lib/foundation-5.3.3/icons/foundation-icons.css">
	
	<script type="text/javascript" src="/common2/lib/jQuery/jquery-2.1.0.min.js"></script>
	<script type="text/javascript" src="lib/d3/d3.v3.min.js"></script>
	
	<script type="text/javascript" src="js/utils.js"></script>
	<script type="text/javascript" src="lib/e4/e4.js"></script>
	<script type="text/javascript" src="lib/e4/e4_chart_helpers.js"></script>
	<script type="text/javascript" src="lib/e4/e4_ranks.js"></script>
	<script type="text/javascript" src="lib/e4/e4_arrow.js"></script>
	
	<script type="text/javascript" src="js/ranks_arrow.js"></script>
	<script type="text/javascript" src="js/ranks_app.js"></script>
	<style>
		#controlPanel label, #controlPanel input {
			display: inline;
		}
	</style>
</head>
<body>
	<div id='viz-container'>
		<div id='controlPanel' class='row' style='background-color: #eeeeee; max-width: 100%;'>
			<div class='small-6 columns' title='comma-separated week#, 0-9'>
				<label>Metric:</label>&nbsp;
				<select id='metric' onchange='app()'>
					<option value='arRatio'>Accepted:Rejected Ratio</option>
					<option value='inflow'>Inflow</option>
					<option value='outflow'>Outflow</option>
				</select>
				
				<br />
				<label for='colsArr'>Reporting Weeks</label>
				<input type='text' name='colsArr' id='colsArr' value='<?php echo $prevWeek .",". $currWeek ?>' onchange='app()' class='small' />				
			</div>
			
			
			
			<div class='small-6 columns'>
				<div>
					<label>Set Range by:</label><br />
					<input type='radio' name='rangeBy' id='rangeBy-minMax' value='minMax' onchange='app()' class='small' checked='checked'/>
					<label for='rangeBy-minMax'>Min,Max</label>
					<input type='radio' name='rangeBy' id='rangeBy-brandId' value='brandId' onchange='app()' class='small' />
					<label for='rangeBy-brandId'>Brand Name</label>
				</div>
				
				<div id='rankMinMaxBox'>
					<!--<label for='colsArr'>Rank Range</label>-->
					<div class='row'>
						<div class='small-6 columns'>
							<label for='rankMin'>Min</label>
							<input type='text' name='rankMin' id='rankMin' value='1' onchange='app()' style='width: 60px' />
						</div>
						<div class='small-6 columns'>
							<label for='rankMax'>Max</label>
							<input type='text' name='rankMax' id='rankMax' value='10' onchange='app()' style='width: 60px' />
						</div>
					</div>
				</div>
			
				<div id='idSearchValBox'>
					<!--<label for='idSearchVal'>Search for brand name&nbsp;</label>-->
					<input type='text' name='nameSearchVal' id='nameSearchVal' value='amazon' onchange='app.getBrandId()' class='small'/>
					<input type='hidden' name='idSearchVal' id='idSearchVal' value='3' onchange='app()' class='small'/>				
				</div>
			</div>
		</div>
		<div id='arrow'></div>
		<div id='colorBins'></div>
		<div id='toolTip'></div>
	</div>
	<script>
		var app = ranks_app("<?php echo TATAG_DOMAIN ?>", {
			metric: 'arRatio',
			rankBy: 'minMax',
			nameSearchVal: 'amazon',
			idSearchVal: 0,
			colsArr: [<?php echo $prevWeek .",". $currWeek ?>],
			rankMin: 0,
			rankMax: 9,
			controlsDisplay: ''
		});
		
		app
		.wrappers({ 
			'arrow': {width: 900, height: 450, margin: {top: 30, left: 100, bottom: 0, right: 0}}
		})
		.viz({
			arrow: ranks_arrow(app)
		});
	
	</script>	
</body>
</html>