<?php
include_once "config.php";
$week = isset($_GET['week']) ? $_GET['week'] : date('W');

?><!DOCTYPE html>
<html>
<head>
	<title>VizChord</title>
	<meta name="viewport" content="width=device-width, initial-scale=1" />
	<link rel="icon" type="image/png" href="/ui/css/logo5.png" />	
	<meta http-equiv="Content-Type" content="text/html;charset=utf-8"/>
	
	<link rel="stylesheet" href="css/normalize.css">
	<link rel="stylesheet" href="/common2/lib/foundation-5.3.3/css/foundation.min.css">
	<link rel="stylesheet" href="/common2/lib/foundation-5.3.3/icons/foundation-icons.css">
	
	<style type="text/css">
	body {
		text-align: center;
	}
	
	#circle circle {
		fill: none;
		pointer-events: all;
	}

	.group path {
		fill-opacity: .5;
	}

	path.chord {
		stroke: #000;
		stroke-width: .25px;
	}

	#circle:hover path.fade {
		display: none;
	}
	
	#toolTip {
		width: 80%; 
		/*height: 100px;*/
		position: fixed;
		margin: 10px;
		padding: 5px; 
		display: none; 
		background-color: rgba(255,255,255,0.95);
		border: 1px solid red;
		border-radius: 2px;
		text-align: left;
		font-size: 12px;
	}
	</style>
</head>
<body>
	<div>
		<br />
		<label for='weekNum' style='display: inline;'>Reporting Week&nbsp;&nbsp;</label>
		<input type='text' name='weekNum' id='weekNum' value="<?php echo $week ?>" onchange='chord()' style='width:100px; display: inline;' />
	</div>
	<h4>Transactions between brands</h4>
	<div id='chord' class='flowDiv'></div>
	<div id='toolTip'></div>
	
	<script type="text/javascript" src="/common2/lib/jQuery/jquery-1.8.2.min.js"></script>
	<script type="text/javascript" src="lib/d3/d3.v3.min.js"></script>
	<script type="text/javascript" src="js/chord_m.js"></script>
	<script type="text/javascript" src="lib/colorbrewer.js"></script>
	<script type="text/javascript" src="js/utils.js"></script>
	<script>
		var params = $.extend({
				weekNum: 32,
				db: ''
			}, $.getParams()
		);
		
		var chord = chord_m("<?php echo TATAG_DOMAIN ?>", '#chord', function () {			
			return "<?php echo TATAG_DOMAIN ?>/flow/matrix?weekNum="+ params.weekNum + '&db=' + params.db
		});

		chord.ready();
		
	</script>
  </body>
</html>